package ru.hhda;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.hhda.core.HeapAnalyzerCore;
import ru.hhda.core.ProcessingState;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.InstanceDatabaseService;
import ru.hhda.core.database.RefDatabaseService;
import ru.hhda.core.database.model.InstanceDump;
import ru.hhda.core.processing.instance.ClassInstanceParser;
import ru.hhda.core.processing.instance.field.ClassInstance;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    public static final int MILLIS_BETWEEN_CHECKS = 1000;

    /**
     * https://java.net/downloads/heap-snapshot/hprof-binary-format.html
     * Or refer to doc/HPROF Agent.html
     */
    public static void main(String[] args) throws Exception {
        File heapDumpFile = new File("D:\\Coding\\heap\\fail.hprof");
        File databaseFile = new File("D:\\Coding\\heap\\fail.hprof.db");
        HeapAnalyzerCore heapAnalyzerCore = HeapAnalyzerCore.init(heapDumpFile, databaseFile);

        AtomicLong counter = new AtomicLong();
        new Thread(() -> {
            long startTime = System.currentTimeMillis();
            NumberFormat formatter = new DecimalFormat("#0.0000");
            ProcessingState state;
            do {
                state = heapAnalyzerCore.getProcessingService().getProcessingState();
                System.out.println("Checks: " + counter.getAndIncrement());
                System.out.println("Stage: " + state.getStage());
                System.out.println("Completion: " + formatter.format(state.getCompletion() * 100) + "%");
                System.out.println();
                try {
                    Thread.sleep(MILLIS_BETWEEN_CHECKS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            } while (state.getStage() != ProcessingStage.COMPLETED);
            System.out.println("Completed in " + (System.currentTimeMillis() - startTime) / 1000 + " seconds.");
        }).start();

        heapAnalyzerCore.getProcessingService().startProcessing();
    }

    static void interactiveShowDumps(AnnotationConfigApplicationContext applicationContext) {
        try {
            InstanceDatabaseService instanceDatabaseService = applicationContext.getBean(InstanceDatabaseService.class);
            RefDatabaseService refDatabaseService = applicationContext.getBean(RefDatabaseService.class);
            ClassInstanceParser classInstanceParser = applicationContext.getBean(ClassInstanceParser.class);
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                System.out.println("Enter instance dump id:");
                String idArg = reader.readLine();
                long id;
                try {
                    id = Long.parseLong(idArg);
                } catch (NumberFormatException e) {
                    System.out.println("Is not a number (" + e + ")");
                    continue;
                }

                TagId tagId = new TagId(id);
                InstanceDump instanceDump = instanceDatabaseService.getInstanceDump(tagId);
                ClassInstance classInstance = classInstanceParser.parseInstance(instanceDump);
                if (classInstance == null) {
                    System.out.println("Not found");
                    System.out.println();
                    System.out.println();
                }
                System.out.println(classInstance.getClassName());
                System.out.println("Fields:");
                classInstance.getFields().forEach(field -> System.out.println(field.getFieldName() + " " + field.getVisibleValue()));
                System.out.println();
                System.out.println("Incoming refs:");
                Collection<TagId> refs = refDatabaseService.getIncomingRefs(classInstance.getInstanceId());
                refs.forEach(refId -> System.out.println(refId));
                System.out.println("Outgoing refs:");
                refs = refDatabaseService.getOutgoingRefs(classInstance.getInstanceId());
                refs.forEach(refId -> System.out.println(refId));
                System.out.println();
                System.out.println();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
