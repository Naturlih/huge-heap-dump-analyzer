package ru.hhda.core.database.model;

import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StringInsertTask implements InsertTask {
    private final TagId id;
    private final String value;

    public StringInsertTask(TagId id, String value) {
        this.id = id;
        this.value = value;
    }

    public TagId getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "StringInsertTask{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }
}
