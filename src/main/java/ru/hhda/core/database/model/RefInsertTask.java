package ru.hhda.core.database.model;

import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class RefInsertTask implements InsertTask {
    private final TagId from;
    private final TagId to;

    public RefInsertTask(TagId from, TagId to) {
        this.from = from;
        this.to = to;
    }

    public TagId getFrom() {
        return from;
    }

    public TagId getTo() {
        return to;
    }

    @Override
    public String toString() {
        return "RefInsertTask{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
