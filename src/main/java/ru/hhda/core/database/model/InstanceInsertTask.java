package ru.hhda.core.database.model;

import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class InstanceInsertTask implements InsertTask {
    private final TagId id;
    private final HeapTagType heapTagType;
    private final long offset;

    public InstanceInsertTask(TagId id, HeapTagType heapTagType, long offset) {
        this.id = id;
        this.heapTagType = heapTagType;
        this.offset = offset;
    }

    public TagId getId() {
        return id;
    }

    public HeapTagType getHeapTagType() {
        return heapTagType;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "InstanceInsertTask{" +
                "id=" + id +
                ", heapTagType=" + heapTagType +
                ", offset=" + offset +
                '}';
    }
}
