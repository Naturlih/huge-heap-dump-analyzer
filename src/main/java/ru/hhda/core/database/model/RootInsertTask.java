package ru.hhda.core.database.model;

import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class RootInsertTask implements InsertTask {
    private final TagId id;
    private final HeapTagType heapTagType;

    public RootInsertTask(TagId id, HeapTagType heapTagType) {
        this.id = id;
        this.heapTagType = heapTagType;
    }

    public TagId getId() {
        return id;
    }

    public HeapTagType getHeapTagType() {
        return heapTagType;
    }

    @Override
    public String toString() {
        return "RootInsertTask{" +
                "id=" + id +
                ", heapTagType=" + heapTagType +
                '}';
    }
}
