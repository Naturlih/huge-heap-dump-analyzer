package ru.hhda.core.database.model;

import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class InstanceDump {
    private final TagId objectId;
    private final HeapTagType heapTagType;
    private final long offset;

    public InstanceDump(TagId objectId, HeapTagType heapTagType, long offset) {
        this.objectId = objectId;
        this.heapTagType = heapTagType;
        this.offset = offset;
    }

    public TagId getObjectId() {
        return objectId;
    }

    public HeapTagType getHeapTagType() {
        return heapTagType;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "InstanceDump{" +
                "objectId=" + objectId +
                ", heapTagType=" + heapTagType +
                ", offset=" + offset +
                '}';
    }
}
