package ru.hhda.core.database;

import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.inserter.AsynchronousInserter;
import ru.hhda.core.database.model.RootInsertTask;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class RootDatabaseService {
    private final AsynchronousInserter inserter;

    public RootDatabaseService(AsynchronousInserter inserter) {
        this.inserter = inserter;
    }

    public void addInstance(HeapTag heapTag) {
        inserter.add(new RootInsertTask(heapTag.getId(), heapTag.getHeapTagType()));
    }

    public void finishStage(ProcessingStage stage) {
        if (stage == ProcessingStage.FINALIZING_HEAP) {
            inserter.finishInserting();
        }
    }
}
