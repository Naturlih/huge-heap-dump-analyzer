package ru.hhda.core.database;

import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.depot.RefDatabaseDepot;
import ru.hhda.core.database.inserter.AsynchronousInserter;
import ru.hhda.core.database.model.RefInsertTask;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.sql.SQLException;
import java.util.Collection;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class RefDatabaseService {
    private final AsynchronousInserter inserter;
    private final RefDatabaseDepot refDatabaseDepot;

    public RefDatabaseService(AsynchronousInserter inserter, RefDatabaseDepot refDatabaseDepot) {
        this.inserter = inserter;
        this.refDatabaseDepot = refDatabaseDepot;
    }

    public void addRef(TagId fromId, TagId toId) {
        if (fromId.getId() == 0 || toId.getId() == 0) {
            return;
        }

        inserter.add(new RefInsertTask(fromId, toId));
    }

    public Collection<TagId> getIncomingRefs(TagId instanceId) {
        try {
            return refDatabaseDepot.getIncomingRefs(instanceId);
        } catch (SQLException e) {
            throw new RuntimeException(e); //TODO
        }
    }

    public Collection<TagId> getOutgoingRefs(TagId instanceId) {
        try {
            return refDatabaseDepot.getOutgoingRefs(instanceId);
        } catch (SQLException e) {
            throw new RuntimeException(e); //TODO
        }
    }

    public void finishStage(ProcessingStage stage) {
        try {
            if (stage == ProcessingStage.FINALIZING_HEAP) {
                refDatabaseDepot.finish();
                inserter.finishInserting();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
