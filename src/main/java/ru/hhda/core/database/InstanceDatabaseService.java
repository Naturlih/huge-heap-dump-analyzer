package ru.hhda.core.database;

import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.depot.InstanceDatabaseDepot;
import ru.hhda.core.database.inserter.AsynchronousInserter;
import ru.hhda.core.database.model.InstanceDump;
import ru.hhda.core.database.model.InstanceInsertTask;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class InstanceDatabaseService {
    private final AsynchronousInserter inserter;
    private final InstanceDatabaseDepot instanceDatabaseDepot;

    public InstanceDatabaseService(AsynchronousInserter inserter, InstanceDatabaseDepot instanceDatabaseDepot) {
        this.inserter = inserter;
        this.instanceDatabaseDepot = instanceDatabaseDepot;
    }

    public void addInstance(HeapTag heapTag) {
        inserter.add(new InstanceInsertTask(heapTag.getId(), heapTag.getHeapTagType(), heapTag.getOffset()));
    }

    public InstanceDump getInstanceDump(TagId tagId) {
        try {
            return instanceDatabaseDepot.getInstanceDump(tagId);
        } catch (SQLException e) {
            throw new RuntimeException(e); //TODO
        }
    }

    public void finishStage(ProcessingStage stage) {
        try {
            if (stage == ProcessingStage.FINALIZING_HEAP) {
                instanceDatabaseDepot.finish();
                inserter.finishInserting();
            }
        } catch (SQLException e) {
            e.printStackTrace(); //TODO
        }
    }
}
