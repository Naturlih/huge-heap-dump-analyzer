package ru.hhda.core.database.depot;

import org.springframework.stereotype.Component;
import ru.hhda.core.database.model.InstanceDump;
import ru.hhda.core.database.model.InstanceInsertTask;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class InstanceDatabaseDepot extends AbstractBulkDatabaseDepot<InstanceInsertTask> {
    private final Connection conn;

    public InstanceDatabaseDepot(Connection connection) {
        this.conn = connection;
    }

    @PostConstruct
    public void init() throws SQLException {
        DatabaseDepotUtil.execute("CREATE TABLE instanceDump (\n" +
                "    objectId BIGINT NOT NULL,\n" +
                "    tagType BYTE NOT NULL,\n" +
                "    offset BIGINT NOT NULL\n" +
                ")", conn);
        super.init();
    }

    @Override
    void setArgs(InstanceInsertTask value, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, value.getId().getId());
        preparedStatement.setLong(2, value.getHeapTagType().getId());
        preparedStatement.setLong(3, value.getOffset());
    }

    @Override
    void finishInserts() throws SQLException {
        DatabaseDepotUtil.execute("CREATE INDEX instanceDump_objectId ON instanceDump (objectId)", conn);
    }

    public InstanceDump getInstanceDump(TagId instanceId) throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            try (PreparedStatement selectInstanceDumpStatement = conn.prepareStatement("SELECT objectId, tagType, offset from instanceDump" +
                    " where objectId = ?")) {
                selectInstanceDumpStatement.setLong(1, instanceId.getId());

                ResultSet rs = selectInstanceDumpStatement.executeQuery();
                InstanceDump result = null;
                if (rs.next()) {
                    TagId objectId = new TagId(rs.getLong("objectId"));
                    HeapTagType basicType = HeapTagType.fromId(rs.getByte("tagType"));
                    long offset = rs.getLong("offset");
                    result = new InstanceDump(objectId, basicType, offset);
                }

                return result;
            }
        }
    }

    PreparedStatement prepareStatement() throws SQLException {
        return conn.prepareStatement("INSERT OR IGNORE INTO instanceDump (objectId, tagType, offset)" +
                " VALUES (?, ?, ?)");
    }
}
