package ru.hhda.core.database.depot;

import org.springframework.stereotype.Component;
import ru.hhda.core.database.model.RefInsertTask;
import ru.hhda.core.tokenizer.tag.model.TagId;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class RefDatabaseDepot extends AbstractBulkDatabaseDepot<RefInsertTask> {
    private Connection conn;

    public RefDatabaseDepot(Connection connection) {
        this.conn = connection;
    }

    @PostConstruct
    public void init() throws SQLException {
        DatabaseDepotUtil.execute("CREATE TABLE ref (\n" +
                "    refFrom BIGINT NOT NULL,\n" +
                "    refTo BIGINT NOT NULL\n" +
                ")", conn);
        super.init();
    }

    @Override
    PreparedStatement prepareStatement() throws SQLException {
        return conn.prepareStatement("INSERT INTO ref (refFrom, refTo) VALUES (?, ?)");
    }

    @Override
    void setArgs(RefInsertTask value, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, value.getFrom().getId());
        preparedStatement.setLong(2, value.getTo().getId());
    }

    @Override
    void finishInserts() throws SQLException {
        DatabaseDepotUtil.execute("CREATE INDEX ref_refFrom ON ref (refFrom)", conn);
        DatabaseDepotUtil.execute("CREATE INDEX ref_refTo ON ref (refTo)", conn);
    }

    public Collection<TagId> getIncomingRefs(TagId instanceId) throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            Collection<TagId> refs = new ArrayList<>();
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT refFrom FROM ref WHERE refTo = ?");
            preparedStatement.setLong(1, instanceId.getId());

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                refs.add(new TagId(rs.getLong("refFrom")));
            }
            preparedStatement.close();

            return refs;
        }
    }

    public Collection<TagId> getOutgoingRefs(TagId instanceId) throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            Collection<TagId> refs = new ArrayList<>();
            PreparedStatement preparedStatement = conn.prepareStatement("SELECT refTo FROM ref WHERE refFrom = ?");
            preparedStatement.setLong(1, instanceId.getId());

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                refs.add(new TagId(rs.getLong("refTo")));
            }
            preparedStatement.close();

            return refs;
        }
    }
}
