package ru.hhda.core.database.depot;

import org.springframework.stereotype.Component;
import ru.hhda.core.database.model.RootInsertTask;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class RootDatabaseDepot extends AbstractBulkDatabaseDepot<RootInsertTask> {
    private Connection conn;

    public RootDatabaseDepot(Connection conn) {
        this.conn = conn;
    }

    @PostConstruct
    public void init() throws SQLException {
        DatabaseDepotUtil.execute("CREATE TABLE heapRoot (\n" +
                "    objectId BIGINT NOT NULL,\n" +
                "    tagType BYTE NOT NULL\n" +
                ")", conn);
        super.init();
    }

    @Override
    PreparedStatement prepareStatement() throws SQLException {
        return conn.prepareStatement("INSERT OR IGNORE INTO heapRoot (objectId, tagType)" +
                " VALUES (?, ?)");
    }

    @Override
    void setArgs(RootInsertTask value, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, value.getId().getId());
        preparedStatement.setLong(2, value.getHeapTagType().getId());
    }

    @Override
    void finishInserts() throws SQLException {}
}
