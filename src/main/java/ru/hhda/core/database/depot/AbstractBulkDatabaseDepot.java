package ru.hhda.core.database.depot;

import ru.hhda.core.database.model.InsertTask;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public abstract class AbstractBulkDatabaseDepot<T extends InsertTask> implements DatabaseDepot<T> {
    private static final int BATCH_SIZE = 1000;

    private long batchSize;
    private PreparedStatement preparedStatement;

    abstract PreparedStatement prepareStatement() throws SQLException;
    abstract void setArgs(T value, PreparedStatement preparedStatement) throws SQLException;
    abstract void finishInserts() throws SQLException;

    void init() throws SQLException {
        preparedStatement = prepareStatement();
    }

    @Override
    public final void insert(T value) throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            if (preparedStatement.isClosed()) {
                preparedStatement = prepareStatement();
            }
            setArgs(value, preparedStatement);
            preparedStatement.addBatch();
            batchSize++;
            if (batchSize > BATCH_SIZE) {
                preparedStatement.executeBatch();
                preparedStatement.close();
                batchSize = 0;
            }
        }
    }

    @Override
    public final void finish() throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            if (batchSize > 0) {
                preparedStatement.executeBatch();
                batchSize = 0;
            }
            if (!preparedStatement.isClosed()) {
                preparedStatement.close();
            }
            finishInserts();
        }
    }
}
