package ru.hhda.core.database.depot;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
class DatabaseDepotUtil {
    static void execute(String sql, Connection conn) throws SQLException {
        try (Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        }
    }
}
