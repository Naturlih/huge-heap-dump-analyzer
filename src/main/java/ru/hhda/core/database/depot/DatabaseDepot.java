package ru.hhda.core.database.depot;

import ru.hhda.core.database.model.InsertTask;

import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface DatabaseDepot<T extends InsertTask> {
    void insert(T value) throws SQLException;

    void finish() throws SQLException;
}
