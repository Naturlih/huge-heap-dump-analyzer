package ru.hhda.core.database.depot;

import org.springframework.stereotype.Component;
import ru.hhda.core.database.model.StringInsertTask;
import ru.hhda.core.tokenizer.tag.model.TagId;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class StringDatabaseDepot extends AbstractBulkDatabaseDepot<StringInsertTask> {
    private final Connection conn;

    public StringDatabaseDepot(Connection conn) {
        this.conn = conn;
    }

    @PostConstruct
    public void init() throws SQLException {
        DatabaseDepotUtil.execute("\n" +
                "CREATE TABLE strings (\n" +
                "    id BIGINT NOT NULL PRIMARY KEY,\n" +
                "    value TEXT\n" +
                ")", conn);
        DatabaseDepotUtil.execute("PRAGMA synchronous = OFF", conn); //TODO
        DatabaseDepotUtil.execute("PRAGMA journal_mode = OFF", conn); //TODO
        super.init();
    }

    @Override
    PreparedStatement prepareStatement() throws SQLException {
        return conn.prepareStatement("INSERT INTO strings (id, value) VALUES (?, ?)");
    }

    @Override
    void setArgs(StringInsertTask value, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setLong(1, value.getId().getId());
        preparedStatement.setString(2, value.getValue());
    }

    @Override
    void finishInserts() throws SQLException {}

    public String getString(TagId tagId) throws SQLException {
        synchronized (DepotSynchronizer.lock) {
            try (PreparedStatement preparedStatement = conn.prepareStatement("SELECT value FROM strings WHERE id = ?")) {
                preparedStatement.setLong(1, tagId.getId());
                ResultSet rs = preparedStatement.executeQuery();
                String result = null;
                if (rs.next()) {
                    result = rs.getString("value");
                }

                return result;
            }
        }
    }
}
