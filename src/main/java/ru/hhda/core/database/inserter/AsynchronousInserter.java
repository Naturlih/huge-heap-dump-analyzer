package ru.hhda.core.database.inserter;

import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.depot.*;
import ru.hhda.core.database.model.*;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class AsynchronousInserter implements Runnable {
    private static final int TRANSACTION_SIZE = 50000;

    private final LinkedBlockingDeque<InsertTask> addItemQueue;
    private final Map<Class<? extends InsertTask>, DatabaseDepot> taskToDepotMap;
    private final Map<ProcessingStage, CountDownLatch> stageToFinishedInsertsLatchMap;
    private final CountDownLatch finishedInsertsLatch;
    private final Connection conn;

    private long insertedCount;
    private boolean transactionIsOpened = false;
    private volatile boolean finishedInserts = false;
    private volatile ProcessingStage stageRequiredToFinish = null;

    @PostConstruct
    void init() {
        Thread databaseFlushThread = new Thread(this);
        databaseFlushThread.setName("Transactional bulk inserter");
        databaseFlushThread.setDaemon(true);
        databaseFlushThread.start();
    }

    public AsynchronousInserter(InstanceDatabaseDepot instanceDatabaseDepot, RefDatabaseDepot refDatabaseDepot,
                                RootDatabaseDepot rootDatabaseDepot, StringDatabaseDepot stringDatabaseDepot, Connection connection) {
        this.addItemQueue = new LinkedBlockingDeque<>(100000);
        this.taskToDepotMap = new HashMap<>();
        this.taskToDepotMap.put(InstanceInsertTask.class, instanceDatabaseDepot);
        this.taskToDepotMap.put(RefInsertTask.class, refDatabaseDepot);
        this.taskToDepotMap.put(RootInsertTask.class, rootDatabaseDepot);
        this.taskToDepotMap.put(StringInsertTask.class, stringDatabaseDepot);
        this.stageToFinishedInsertsLatchMap = new HashMap<>();
        for (ProcessingStage stage : ProcessingStage.values()) {
            this.stageToFinishedInsertsLatchMap.put(stage, new CountDownLatch(1));
        }
        this.conn = connection;
        this.finishedInsertsLatch = new CountDownLatch(1);
    }

    public void add(InsertTask value) {
        try {
            addItemQueue.putFirst(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void run() {
        while (!finishedInserts || !addItemQueue.isEmpty()) {
            InsertTask valueToInsert = addItemQueue.pollLast();
            if (valueToInsert == null) {
                if (stageRequiredToFinish != null) {
                    stageToFinishedInsertsLatchMap.get(stageRequiredToFinish).countDown();
                    stageRequiredToFinish = null;
                }
                if (finishedInserts) {
                    break;
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }

                continue;
            }


            try {
                openTransactionIfNeeded();
                taskToDepotMap.get(valueToInsert.getClass()).insert(valueToInsert);
                insertedCount++;
                closeTransactionIfNeeded();
            } catch (SQLException e) { //TODO
                e.printStackTrace();
            }
        }

        try {
            closeTransaction();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finishedInsertsLatch.countDown();
    }

    public void finishStage(ProcessingStage stage) {
        stageRequiredToFinish = stage;
        try {
            stageToFinishedInsertsLatchMap.get(stage).await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    public void finishInserting() {
        finishedInserts = true;
        try {
            finishedInsertsLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    private void openTransactionIfNeeded() throws SQLException {
        if (!transactionIsOpened) {
            openTransaction();
            transactionIsOpened = true;
        }

    }

    private void closeTransactionIfNeeded() throws SQLException {
        if (transactionIsOpened && insertedCount > TRANSACTION_SIZE) {
            closeTransaction();
            transactionIsOpened = false;
            insertedCount = 0;
        }
    }

    private void openTransaction() throws SQLException {
        try (Statement statement = conn.createStatement()) {
            statement.execute("BEGIN TRANSACTION");
        } catch (SQLException e) {
            if (!e.getMessage().contains("cannot start a transaction within a transaction")) {
                e.printStackTrace();//TODO
            }
        }
    }

    private void closeTransaction() throws SQLException {
        try (Statement statement = conn.createStatement()) {
            statement.execute("COMMIT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
