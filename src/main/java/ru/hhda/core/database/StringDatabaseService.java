package ru.hhda.core.database;

import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.depot.StringDatabaseDepot;
import ru.hhda.core.database.inserter.AsynchronousInserter;
import ru.hhda.core.database.model.StringInsertTask;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class StringDatabaseService {
    private final AsynchronousInserter inserter;
    private final StringDatabaseDepot stringDatabaseDepot;

    public StringDatabaseService(AsynchronousInserter inserter, StringDatabaseDepot stringDatabaseDepot) {
        this.inserter = inserter;
        this.stringDatabaseDepot = stringDatabaseDepot;
    }

    public void addString(TagId stringId, String value) {
        inserter.add(new StringInsertTask(stringId, value));
    }

    public String getString(TagId stringId) {
        try {
            String string = stringDatabaseDepot.getString(stringId);
            if (string == null) {
//                throw new IllegalArgumentException("Cannot find string by id " + stringId);
                return "<<<undefined>>>";
            }

            return string;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void finishStage(ProcessingStage stage) {
        if (stage == ProcessingStage.FINALIZING_META) {
            try {
                stringDatabaseDepot.finish();
            } catch (SQLException e) {
                e.printStackTrace(); //TODO
            }
            inserter.finishStage(stage);
        }
        if (stage == ProcessingStage.FINALIZING_HEAP) {
            inserter.finishInserting();
        }
    }
}
