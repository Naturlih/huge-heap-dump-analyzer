package ru.hhda.core.tokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.tokenizer.tag.*;
import ru.hhda.core.tokenizer.tag.model.*;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Is not thread safe.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class TagFetcher {
    @Autowired
    private final HeapReader heapReader;
    @Autowired
    private final ReaderContext readerContext;

    TagFetcher(HeapReader heapReader, ReaderContext readerContext) {
        this.heapReader = heapReader;
        this.readerContext = readerContext;
    }

    public Tag getTag() throws IOException {
        TagMeta tagMeta = getTagMeta();
        Tag tag;
        switch (tagMeta.getTagType()) {
            case STRING_IN_UTF_8:
                tag = fetchStringInUtf8Tag(tagMeta);
                break;
            case LOAD_CLASS:
                tag = fetchLoadClassTag(tagMeta);
                break;
            case UNLOAD_CLASS:
                tag = fetchUnloadClassTag(tagMeta);
                break;
            case STACK_FRAME:
                tag = fetchStackFrameTag(tagMeta);
                break;
            case STACK_TRACE:
                tag = fetchStackTraceTag(tagMeta);
                break;
            case ALLOC_SITES:
                tag = fetchAllocSitesTag(tagMeta);
                break;
            case HEAP_SUMMARY:
                tag = fetchHeapSummaryTag(tagMeta);
                break;
            case START_THREAD:
                tag = fetchStartThreadTag(tagMeta);
                break;
            case END_THREAD:
                tag = fetchEndThreadTag(tagMeta);
                break;
            case HEAP_DUMP:
                tag = fetchHeapDumpTag(tagMeta);
                break;
            case HEAP_DUMP_SEGMENT:
                tag = fetchHeapDumpSegmentTag(tagMeta);
                break;
            case HEAP_DUMP_END:
                tag = fetchHeapDumpEndTag(tagMeta);
                break;
            case CPU_SAMPLES:
                tag = fetchCpuSamplesTag(tagMeta);
                break;
            case CONTROL_SETTINGS:
                tag = fetchControlSettingsTag(tagMeta);
                break;
            default:
                throw new IllegalStateException("Unexpected tag type for tag meta " + tagMeta);
        }

        return tag;
    }

    private Tag fetchStringInUtf8Tag(TagMeta tagMeta) throws IOException {
        TagId id = heapReader.readId();
        int length = (int) tagMeta.getLength() - readerContext.getIdentifierSize();
        byte[] utf8Bytes = heapReader.read(length);

        return new StringInUtf8Tag(tagMeta, id, new String(utf8Bytes, Charset.forName("UTF-8")));
    }

    private Tag fetchLoadClassTag(TagMeta tagMeta) throws IOException {
        long classSerialNumber = heapReader.readU4();
        TagId classObjectId = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        TagId classNameStringId = heapReader.readId();

        return new LoadClassTag(tagMeta, classSerialNumber, classObjectId, stackTraceSerialNumber, classNameStringId);
    }

    private Tag fetchUnloadClassTag(TagMeta tagMeta) throws IOException {
        long classSerialNumber = heapReader.readU4();

        return new UnloadClassTag(tagMeta, classSerialNumber);
    }

    private Tag fetchStackFrameTag(TagMeta tagMeta) throws IOException {
        TagId stackFrameId = heapReader.readId();
        TagId methodNameStringId = heapReader.readId();
        TagId methodSignatureStringId = heapReader.readId();
        TagId sourceFileNameStringId = heapReader.readId();
        long classSerialNumber = heapReader.readU4();
        long lineNumber = heapReader.readU4();

        return new StackFrameTag(tagMeta, stackFrameId, methodNameStringId, methodSignatureStringId,
                sourceFileNameStringId, classSerialNumber, lineNumber);
    }

    private Tag fetchStackTraceTag(TagMeta tagMeta) throws IOException {
        long stackTraceSerialNumber = heapReader.readU4();
        long threadSerialNumber = heapReader.readU4();
        int numberOfFrames = (int) heapReader.readU4();
        TagId[] stackFrameIds = new TagId[numberOfFrames];
        for (int i = 0; i < numberOfFrames; i++) {
            stackFrameIds[i] = heapReader.readId();
        }

        return new StackTraceTag(tagMeta, stackTraceSerialNumber, threadSerialNumber, numberOfFrames, stackFrameIds);
    }

    private Tag fetchAllocSitesTag(TagMeta tagMeta) throws IOException {
        int flags = heapReader.readU2();
        long cutoffRatio = heapReader.readU4();
        long totalLiveBytes = heapReader.readU4();
        long totalLiveInstances = heapReader.readU4();
        long totalBytesAllocated = heapReader.readU8();
        long totalInstancesAllocated = heapReader.readU8();
        int sitesCount = (int) heapReader.readU4();
        AllocSite[] allocSites = new AllocSite[sitesCount];
        for (int i = 0; i < sitesCount; i++) {
            byte arrayTypeId = (byte) heapReader.readU1();
            BasicType basicType = BasicType.fromId(arrayTypeId);
            if (basicType == null) {
                throw new IllegalStateException("Cannot find array type for id " + arrayTypeId);
            }
            long classSerialNumber = heapReader.readU4();
            long stackTraceSerialNumber = heapReader.readU4();
            long liveBytes = heapReader.readU4();
            long liveInstances = heapReader.readU4();
            long bytesAllocated = heapReader.readU8();
            long instancesAllocated = heapReader.readU8();

            allocSites[i] = new AllocSite(basicType, classSerialNumber, stackTraceSerialNumber, liveBytes,
                    liveInstances, bytesAllocated, instancesAllocated);
        }

        return new AllocSitesTag(tagMeta, flags, cutoffRatio, totalLiveBytes, totalLiveInstances, totalBytesAllocated,
                totalInstancesAllocated, sitesCount, allocSites);
    }

    private Tag fetchHeapSummaryTag(TagMeta tagMeta) throws IOException {
        long totalLiveBytes = heapReader.readU4();
        long totalLiveInstances = heapReader.readU4();
        long totalBytesAllocated = heapReader.readU8();
        long totalInstancesAllocated = heapReader.readU8();

        return new HeapSummaryTag(tagMeta, totalLiveBytes, totalLiveInstances, totalBytesAllocated, totalInstancesAllocated);
    }

    private Tag fetchStartThreadTag(TagMeta tagMeta) throws IOException {
        long threadSerialNumber = heapReader.readU4();
        TagId threadObjectId = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        TagId threadNameStringId = heapReader.readId();
        TagId threadGroupNameId = heapReader.readId();
        TagId threadParentGroupNameId = heapReader.readId();

        return new StartThreadTag(tagMeta, threadSerialNumber, threadObjectId, stackTraceSerialNumber,
                threadNameStringId, threadGroupNameId, threadParentGroupNameId);
    }

    private Tag fetchEndThreadTag(TagMeta tagMeta) throws IOException {
        long threadSerialNumber = heapReader.readU4();

        return new EndThreadTag(tagMeta, threadSerialNumber);
    }

    private Tag fetchHeapDumpTag(TagMeta tagMeta) throws IOException {
        return new HeapDumpTag(tagMeta);
    }

    private Tag fetchHeapDumpSegmentTag(TagMeta tagMeta) throws IOException {
        return new HeapDumpSegmentTag(tagMeta);
    }

    private Tag fetchHeapDumpEndTag(TagMeta tagMeta) throws IOException {
        return new HeapDumpEndTag(tagMeta);
    }

    private Tag fetchCpuSamplesTag(TagMeta tagMeta) throws IOException {
        int totalNumberOfSamples = (int) heapReader.readU4();
        CpuSampleTrace[] cpuSampleTraces = new CpuSampleTrace[totalNumberOfSamples];
        for (int i = 0; i < totalNumberOfSamples; i++) {
            long numberOfSamples = heapReader.readU4();
            long stackTraceSerialNumber = heapReader.readU4();

            cpuSampleTraces[i] = new CpuSampleTrace(numberOfSamples, stackTraceSerialNumber);
        }

        return new CpuSamplesTag(tagMeta, totalNumberOfSamples, cpuSampleTraces);
    }

    private Tag fetchControlSettingsTag(TagMeta tagMeta) throws IOException {
        long flag = heapReader.readU4();
        int stackTraceDepth = heapReader.readU2();

        return new ControlSettingsTag(tagMeta, flag, stackTraceDepth);
    }

    private TagMeta getTagMeta() throws IOException {
        byte tagTypeId = (byte) heapReader.readU1();
        TagType tagType = TagType.getById(tagTypeId);
        if (tagType == null) {
            throw new IllegalStateException("Cannot read tag type by id " + tagTypeId);
        }
        long time = heapReader.readU4();
        long length = heapReader.readU4();

        return new TagMeta(tagType, time, length);
    }
}
