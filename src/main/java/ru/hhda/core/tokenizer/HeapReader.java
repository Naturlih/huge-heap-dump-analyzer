package ru.hhda.core.tokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.tokenizer.tag.model.TagId;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class HeapReader {
    private BufferedInputStream stream;
    @Autowired
    private ReaderContext readerContext;
    private long offset;

    @PostConstruct
    public void init() throws IOException {
        this.offset = 0;
        this.stream = new BufferedInputStream(new FileInputStream(readerContext.getHeapDumpFile()));
    }

    @PreDestroy
    public void destroy() throws IOException {
        this.stream.close();
    }

    public HeapReader(ReaderContext readerContext) {
        this.readerContext = readerContext;
    }

    public ReaderContext getReaderContext() {
        return readerContext;
    }

    public void reset() throws IOException {
        stream = new BufferedInputStream(new FileInputStream(readerContext.getHeapDumpFile()));
        offset = 0;
        fetchInitialString();
        fetchIdentifiersSize();
        fetchMilliseconds();
    }

    public void close() throws IOException {
        if (stream != null) {
            stream.close();
        }
    }

    TagId readId() throws IOException {
        long id = 0;
        for (int i = 0; i < readerContext.getIdentifierSize(); i++) {
            id = (id << 8) + stream.read();
            offset++;
        }

        return new TagId(id);
    }

    long readU8() throws IOException {
        long u8 = 0;
        for (int i = 0; i < 8; i++) {
            u8 = (u8 << 8) + stream.read();
            offset++;
        }

        return u8;
    }

    long readU4() throws IOException {
        int u4 = 0;

        for (int i = 0; i < 4; i++) {
            u4 = (u4 << 8) + stream.read();
            offset++;
        }

        return u4;
    }

    int readU2() throws IOException {
        short u2 = 0;
        for (int i = 0; i < 2; i++) {
            u2 = (short) ((u2 << 8) + stream.read());
            offset++;
        }

        return u2;
    }

    int readU1() throws IOException {
        int u1 = stream.read();
        offset++;

        return u1;
    }

    /**
     * Tries to read {@code length} bytes. Throws {@code IOException}, if read less than {@code length} bytes.
     *
     * @param length bytes to read
     * @return read bytes
     * @throws IOException if read less than {@code length} bytes.
     */
    byte[] read(int length) throws IOException {
        byte[] arr = new byte[length];

        int bytesRead = stream.read(arr);
        if (bytesRead != length) {
            throw new IOException("Tried to read " + length + " bytes, but stream.read returns " + bytesRead + " bytes.");
        }

        offset += length;
        return arr;
    }

    public void skip(long toSkip) throws IOException {
        long totalSkipped = 0;
        long leftToSkip = toSkip;
        while (totalSkipped != toSkip) {
            long skipped = stream.skip(leftToSkip);
            if (skipped == 0) {
                throw new IllegalStateException("Tried to skip " + toSkip + ", skipped " + totalSkipped
                        + ", and skipped 0 in last stream.skip().");
            }
            totalSkipped += skipped;
            leftToSkip -= skipped;
        }

        offset += totalSkipped;
    }

    public boolean hasMore() {
        return offset < readerContext.getHeapDumpSize();
    }

    public long getOffset() {
        return offset;
    }

    public String fetchInitialString() throws IOException {
        StringBuilder builder = new StringBuilder();
        int c = stream.read();
        offset++;

        while (c != 0) {
            builder.append((char) c);
            c = stream.read();
            offset++;
        }

        return builder.toString();
    }

    public int fetchIdentifiersSize() throws IOException {
        int identifierSize = 0;

        for (int i = 0; i < 4; i++) {
            identifierSize = (identifierSize << 8) + stream.read();
            offset++;
        }

        return identifierSize;
    }

    public long fetchMilliseconds() throws IOException {
        long milliseconds = 0;

        for (int i = 0; i < 8; i++) {
            milliseconds = (milliseconds << 8) + stream.read();
            offset++;
        }

        return milliseconds;
    }
}
