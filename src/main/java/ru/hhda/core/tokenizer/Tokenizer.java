package ru.hhda.core.tokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.tokenizer.tag.Tag;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.model.TagType;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class Tokenizer {
    @Autowired
    private final TagFetcher tagFetcher;
    @Autowired
    private final HeapReader heapReader;
    @Autowired
    private final ReaderContext readerContext;

    private HeapTagFetcher heapTagFetcher;
    private boolean isParsingHeapDump = false;
    private long offsetOfEndOfHeapTag;

    @PostConstruct
    void initReaderContext() throws IOException {
        String initialString = heapReader.fetchInitialString();
        int identifiersSize = heapReader.fetchIdentifiersSize();
        long jvmStartTime = heapReader.fetchMilliseconds();

        this.readerContext.update(initialString, identifiersSize, jvmStartTime);
    }

    public Tokenizer(TagFetcher tagFetcher, HeapReader heapReader, ReaderContext readerContext) {
        this.tagFetcher = tagFetcher;
        this.heapReader = heapReader;
        this.readerContext = readerContext;
    }

    public boolean isParsingHeapDump() {
        return isParsingHeapDump;
    }

    public Tag readTag() throws IOException {
        if (isParsingHeapDump) {
            throw new IllegalStateException("Cannot fetch regular tag while parsing heap dump.");
        }

        Tag tag = tagFetcher.getTag();
        if (tag.getTagMeta().getTagType() == TagType.HEAP_DUMP
                || tag.getTagMeta().getTagType() == TagType.HEAP_DUMP_SEGMENT) {
            isParsingHeapDump = true;
            heapTagFetcher = new HeapTagFetcher(heapReader, readerContext);
            offsetOfEndOfHeapTag = heapReader.getOffset() + tag.getTagMeta().getLength();
        }

        if (!heapReader.hasMore()) {
            isParsingHeapDump = false;
        }

        return tag;
    }

    public HeapTag readHeapTag() throws IOException {
        if (heapTagIsProceed()) {
            isParsingHeapDump = false;
            heapTagFetcher = null;

            return null;
        }

        if (!isParsingHeapDump) {
            throw new IllegalStateException("Cannot fetch heap tag without parsing heap tag.");
        }

        return heapTagFetcher.getTag();
    }

    private boolean heapTagIsProceed() {
        if (!heapReader.hasMore()) {
            return true;
        }

        if (heapReader.getOffset() >= offsetOfEndOfHeapTag) {
            if (offsetOfEndOfHeapTag != heapReader.getOffset()) {
                throw new IllegalStateException("Offset is not at exact end of heap dump tag");
            }

            return true;
        }

        return false;
    }
}
