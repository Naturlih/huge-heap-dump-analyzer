package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootStickyClassHeapTag implements HeapTag {
    private final TagId id;
    private final long offset;

    public RootStickyClassHeapTag(TagId id, long offset) {
        this.id = id;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_STICKY_CLASS;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootStickyClassHeapTag{" +
                "id=" + id +
                '}';
    }
}
