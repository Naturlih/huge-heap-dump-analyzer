package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class CpuSampleTrace {
    private final long numberOfSamples;
    private final long stackTraceSerialNumber;

    public CpuSampleTrace(long numberOfSamples, long stackTraceSerialNumber) {
        this.numberOfSamples = numberOfSamples;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
    }

    public long getNumberOfSamples() {
        return numberOfSamples;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    @Override
    public String toString() {
        return "CpuSampleTrace{" +
                "numberOfSamples=" + numberOfSamples +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                '}';
    }
}
