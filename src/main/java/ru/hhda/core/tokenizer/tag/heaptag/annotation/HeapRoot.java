package ru.hhda.core.tokenizer.tag.heaptag.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks heap root heap tag.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface HeapRoot {
}
