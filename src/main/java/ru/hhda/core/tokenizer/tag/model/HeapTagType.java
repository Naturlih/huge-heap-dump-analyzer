package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public enum HeapTagType {
    ROOT_UNKNOWN(0xFF),
    ROOT_JNI_GLOBAL(0x01),
    ROOT_JNI_LOCAL(0x02),
    ROOT_JAVA_FRAME(0x03),
    ROOT_NATIVE_STACK(0x04),
    ROOT_STICKY_CLASS(0x05),
    ROOT_THREAD_BLOCK(0x06),
    ROOT_MONITOR_USED(0x07),
    ROOT_THREAD_OBJECT(0x08),
    CLASS_DUMP(0x20),
    INSTANCE_DUMP(0x21),
    OBJECT_ARRAY_DUMP(0x22),
    PRIMITIVE_ARRAY_DUMP(0x23);

    private final byte id;

    HeapTagType(int id) {
        this.id = (byte) id;
    }

    public byte getId() {
        return id;
    }

    public static HeapTagType fromId(byte id) {
        for (HeapTagType tagType : values()) {
            if (tagType.getId() == id) {
                return tagType;
            }
        }

        return null;
    }
}
