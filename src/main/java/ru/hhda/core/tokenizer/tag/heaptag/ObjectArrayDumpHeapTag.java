package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.util.Arrays;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@LookupInHeapFile
public class ObjectArrayDumpHeapTag implements HeapTag {
    private final TagId id;
    private final long stackTraceSerialNumber;
    private final int numberOfElements;
    private final TagId arrayClassObjectId;
    private final long offset;
    private final TagId[] elements;

    public ObjectArrayDumpHeapTag(TagId id, long stackTraceSerialNumber, int numberOfElements, TagId arrayClassObjectId, long offset, TagId[] elements) {
        this.id = id;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.numberOfElements = numberOfElements;
        this.arrayClassObjectId = arrayClassObjectId;
        this.offset = offset;
        this.elements = elements;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public TagId getArrayClassObjectId() {
        return arrayClassObjectId;
    }

    public TagId[] getElements() {
        return elements;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.OBJECT_ARRAY_DUMP;
    }

    @Override
    public String toString() {
        return "ObjectArrayDumpHeapTag{" +
                "id=" + id +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", numberOfElements=" + numberOfElements +
                ", arrayClassObjectId=" + arrayClassObjectId +
                ", elements=" + Arrays.toString(elements) +
                '}';
    }
}
