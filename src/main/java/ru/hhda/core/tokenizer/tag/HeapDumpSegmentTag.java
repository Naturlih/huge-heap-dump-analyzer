package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class HeapDumpSegmentTag extends HeapDumpTag {
    public HeapDumpSegmentTag(TagMeta tagMeta) {
        super(tagMeta);
    }
}
