package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagId;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StackFrameTag implements Tag {
    private final TagMeta tagMeta;
    private final TagId stackFrameId;
    private final TagId methodNameStrnigId;
    private final TagId methodSignatureStringId;
    private final TagId sourceFileNameStringId;
    private final long classSerialNumber;
    private final long lineNumber;

    public StackFrameTag(TagMeta tagMeta, TagId stackFrameId, TagId methodNameStrnigId, TagId methodSignatureStringId, TagId sourceFileNameStringId, long classSerialNumber, long lineNumber) {
        this.tagMeta = tagMeta;
        this.stackFrameId = stackFrameId;
        this.methodNameStrnigId = methodNameStrnigId;
        this.methodSignatureStringId = methodSignatureStringId;
        this.sourceFileNameStringId = sourceFileNameStringId;
        this.classSerialNumber = classSerialNumber;
        this.lineNumber = lineNumber;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public TagId getStackFrameId() {
        return stackFrameId;
    }

    public TagId getMethodNameStrnigId() {
        return methodNameStrnigId;
    }

    public TagId getMethodSignatureStringId() {
        return methodSignatureStringId;
    }

    public TagId getSourceFileNameStringId() {
        return sourceFileNameStringId;
    }

    public long getClassSerialNumber() {
        return classSerialNumber;
    }

    public long getLineNumber() {
        return lineNumber;
    }

    @Override
    public String toString() {
        return "StackFrameTag{" +
                "tagMeta=" + tagMeta +
                ", stackFrameId=" + stackFrameId +
                ", methodNameStrnigId=" + methodNameStrnigId +
                ", methodSignatureStringId=" + methodSignatureStringId +
                ", sourceFileNameStringId=" + sourceFileNameStringId +
                ", classSerialNumber=" + classSerialNumber +
                ", lineNumber=" + lineNumber +
                '}';
    }
}
