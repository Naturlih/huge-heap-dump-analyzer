package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@LookupInHeapFile
public class InstanceDumpHeapTag implements HeapTag {
    private final TagId id;
    private final long stackTraceSerialNumber;
    private final TagId classObjectId;
    private final int length;
    private final byte[] value;
    private final long offset;

    public InstanceDumpHeapTag(TagId id, long stackTraceSerialNumber, TagId classObjectId, int length, byte[] value, long offset) {
        this.id = id;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.classObjectId = classObjectId;
        this.length = length;
        this.value = value;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public TagId getClassObjectId() {
        return classObjectId;
    }

    public int getLength() {
        return length;
    }

    public byte[] getValue() {
        return value;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.INSTANCE_DUMP;
    }

    @Override
    public String toString() {
        return "InstanceDumpHeapTag{" +
                "id=" + id +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", classObjectId=" + classObjectId +
                ", length=" + length +
                '}';
    }
}
