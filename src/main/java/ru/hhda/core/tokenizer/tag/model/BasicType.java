package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public enum BasicType {
    NOT_AN_ARRAY(0),
    OBJECT(2),
    BOOLEAN(4),
    CHAR(5),
    FLOAT(6),
    DOUBLE(7),
    BYTE(8),
    SHORT(9),
    INT(10),
    LONG(11);

    private byte id;

    BasicType(int id) {
        byte idAsByte = (byte) id;
        if (id != idAsByte) {
            throw new IllegalArgumentException("Id " + id + " is not byte.");
        }

        this.id = idAsByte;
    }

    public byte getId() {
        return id;
    }

    public static BasicType fromId(byte id) {
        for (BasicType basicType : values()) {
            if (basicType.getId() == id) {
                return basicType;
            }
        }

        return null;
    }
}
