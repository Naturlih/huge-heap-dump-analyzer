package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StaticFieldEntry {
    private final TagId fieldNameId;
    private final BasicType basicType;
    private final long value;

    public StaticFieldEntry(TagId fieldNameId, BasicType basicType, long value) {
        this.fieldNameId = fieldNameId;
        this.basicType = basicType;
        this.value = value;
    }

    public TagId getFieldNameId() {
        return fieldNameId;
    }

    public BasicType getBasicType() {
        return basicType;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "StaticFieldEntry{" +
                "fieldNameId=" + fieldNameId +
                ", basicType=" + basicType +
                ", value=" + value +
                '}';
    }
}
