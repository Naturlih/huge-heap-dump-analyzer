package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class InstanceFieldEntry {
    private final TagId fieldNameId;
    private final BasicType basicType;

    public InstanceFieldEntry(TagId fieldNameId, BasicType basicType) {
        this.fieldNameId = fieldNameId;
        this.basicType = basicType;
    }

    public TagId getFieldNameId() {
        return fieldNameId;
    }

    public BasicType getBasicType() {
        return basicType;
    }

    @Override
    public String toString() {
        return "InstanceFieldEntry{" +
                "fieldNameId=" + fieldNameId +
                ", basicType=" + basicType +
                '}';
    }
}
