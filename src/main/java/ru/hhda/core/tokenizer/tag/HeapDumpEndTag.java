package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class HeapDumpEndTag implements Tag {
    private final TagMeta tagMeta;

    public HeapDumpEndTag(TagMeta tagMeta) {
        this.tagMeta = tagMeta;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    @Override
    public String toString() {
        return "HeapDumpEndTag{" +
                "tagMeta=" + tagMeta +
                '}';
    }
}
