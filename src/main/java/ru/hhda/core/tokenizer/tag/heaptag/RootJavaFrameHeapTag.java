package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootJavaFrameHeapTag implements HeapTag {
    private final TagId id;
    private final long threadSerialNumber;
    private final long frameNumberInStackTrace;
    private final long offset;

    public RootJavaFrameHeapTag(TagId id, long threadSerialNumber, long frameNumberInStackTrace, long offset) {
        this.id = id;
        this.threadSerialNumber = threadSerialNumber;
        this.frameNumberInStackTrace = frameNumberInStackTrace;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    public long getFrameNumberInStackTrace() {
        return frameNumberInStackTrace;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_JAVA_FRAME;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootJavaFrameHeapTag{" +
                "id=" + id +
                ", threadSerialNumber=" + threadSerialNumber +
                ", frameNumberInStackTrace=" + frameNumberInStackTrace +
                '}';
    }
}
