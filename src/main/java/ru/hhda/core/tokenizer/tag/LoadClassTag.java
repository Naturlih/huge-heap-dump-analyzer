package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagId;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class LoadClassTag implements Tag {
    private final TagMeta tagMeta;
    private final long classSerialNumber;
    private final TagId classObjectId;
    private final long stackTraceSerialNumber;
    private final TagId classNameStringId;

    public LoadClassTag(TagMeta tagMeta, long classSerialNumber, TagId classObjectId, long stackTraceSerialNumber, TagId classNameStringId) {
        this.tagMeta = tagMeta;
        this.classSerialNumber = classSerialNumber;
        this.classObjectId = classObjectId;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.classNameStringId = classNameStringId;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getClassSerialNumber() {
        return classSerialNumber;
    }

    public TagId getClassObjectId() {
        return classObjectId;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public TagId getClassNameStringId() {
        return classNameStringId;
    }

    @Override
    public String toString() {
        return "LoadClassTag{" +
                "tagMeta=" + tagMeta +
                ", classSerialNumber=" + classSerialNumber +
                ", classObjectId=" + classObjectId +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", classNameStringId=" + classNameStringId +
                '}';
    }
}
