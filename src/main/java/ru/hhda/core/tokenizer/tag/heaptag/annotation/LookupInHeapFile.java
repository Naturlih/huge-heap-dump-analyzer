package ru.hhda.core.tokenizer.tag.heaptag.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks those heap tags, which content should be retrieved directly from heap (.hprof) file.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LookupInHeapFile {
}
