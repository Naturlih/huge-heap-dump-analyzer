package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface HeapTag {
    TagId getId();
    HeapTagType getHeapTagType();
    long getOffset();
}
