package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootJniGlobalHeapTag implements HeapTag {
    private final TagId id;
    private final TagId jniGlobalRefId;
    private final long offset;

    public RootJniGlobalHeapTag(TagId id, TagId jniGlobalRefId, long offset) {
        this.id = id;
        this.jniGlobalRefId = jniGlobalRefId;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public TagId getJniGlobalRefId() {
        return jniGlobalRefId;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_JNI_GLOBAL;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootJniGlobalHeapTag{" +
                "id=" + id +
                ", jniGlobalRefId=" + jniGlobalRefId +
                '}';
    }
}
