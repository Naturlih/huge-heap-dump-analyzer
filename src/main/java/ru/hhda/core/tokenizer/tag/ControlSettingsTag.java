package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ControlSettingsTag implements Tag {
    private final TagMeta tagMeta;
    private final long flag;
    private final int stackTraceDepth;

    public ControlSettingsTag(TagMeta tagMeta, long flag, int stackTraceDepth) {
        this.tagMeta = tagMeta;
        this.flag = flag;
        this.stackTraceDepth = stackTraceDepth;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getFlag() {
        return flag;
    }

    public int getStackTraceDepth() {
        return stackTraceDepth;
    }

    @Override
    public String toString() {
        return "ControlSettingsTag{" +
                "tagMeta=" + tagMeta +
                ", flag=" + flag +
                ", stackTraceDepth=" + stackTraceDepth +
                '}';
    }
}
