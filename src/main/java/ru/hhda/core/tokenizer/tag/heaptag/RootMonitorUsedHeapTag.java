package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootMonitorUsedHeapTag implements HeapTag {
    private final TagId id;
    private final long offset;

    public RootMonitorUsedHeapTag(TagId id, long offset) {
        this.id = id;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_MONITOR_USED;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootMonitorUsedHeapTag{" +
                "id=" + id +
                '}';
    }
}
