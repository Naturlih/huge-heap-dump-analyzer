package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class TagId {
    private final long id;

    public TagId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "TagId{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TagId tagId = (TagId) o;

        return id == tagId.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
