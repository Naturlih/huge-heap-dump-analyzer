package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class UnloadClassTag implements Tag {
    private final TagMeta tagMeta;
    private final long classSerialNumber;

    public UnloadClassTag(TagMeta tagMeta, long classSerialNumber) {
        this.tagMeta = tagMeta;
        this.classSerialNumber = classSerialNumber;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getClassSerialNumber() {
        return classSerialNumber;
    }

    @Override
    public String toString() {
        return "UnloadClassTag{" +
                "tagMeta=" + tagMeta +
                ", classSerialNumber=" + classSerialNumber +
                '}';
    }
}
