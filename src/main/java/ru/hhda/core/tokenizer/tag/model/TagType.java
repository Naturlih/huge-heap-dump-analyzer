package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public enum TagType {
    STRING_IN_UTF_8(0x01),
    LOAD_CLASS(0x02),
    UNLOAD_CLASS(0x03),
    STACK_FRAME(0x04),
    STACK_TRACE(0x05),
    ALLOC_SITES(0x06),
    HEAP_SUMMARY(0x07),
    START_THREAD(0x0A),
    END_THREAD(0x0B),
    HEAP_DUMP(0x0C),
    HEAP_DUMP_SEGMENT(0x1C),
    HEAP_DUMP_END(0x2C),
    CPU_SAMPLES(0x0D),
    CONTROL_SETTINGS(0x0E),;

    private final byte id;

    TagType(int id) {
        byte idAsByte = (byte) id;
        if (id != idAsByte) {
            throw new IllegalArgumentException("Id " + id + " is not byte.");
        }

        this.id = idAsByte;
    }

    public byte getId() {
        return id;
    }

    public static TagType getById(byte id) {
        for (TagType tagType : values()) {
            if (tagType.getId() == id) {
                return tagType;
            }
        }

        return null;
    }
}
