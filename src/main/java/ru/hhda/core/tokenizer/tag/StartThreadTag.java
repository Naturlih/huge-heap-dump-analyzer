package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagId;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StartThreadTag implements Tag {
    private final TagMeta tagMeta;
    private final long threadSerialNumber;
    private final TagId threadObjectId;
    private final long stackTraceSerialNumber;
    private final TagId threadNameStringId;
    private final TagId threadGroupNameId;
    private final TagId threadParentGroupNameId;

    public StartThreadTag(TagMeta tagMeta, long threadSerialNumber, TagId threadObjectId, long stackTraceSerialNumber,
                          TagId threadNameStringId, TagId threadGroupNameId, TagId threadParentGroupNameId) {
        this.tagMeta = tagMeta;
        this.threadSerialNumber = threadSerialNumber;
        this.threadObjectId = threadObjectId;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.threadNameStringId = threadNameStringId;
        this.threadGroupNameId = threadGroupNameId;
        this.threadParentGroupNameId = threadParentGroupNameId;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    public TagId getThreadObjectId() {
        return threadObjectId;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public TagId getThreadNameStringId() {
        return threadNameStringId;
    }

    public TagId getThreadGroupNameId() {
        return threadGroupNameId;
    }

    public TagId getThreadParentGroupNameId() {
        return threadParentGroupNameId;
    }

    @Override
    public String toString() {
        return "StartThreadTag{" +
                "tagMeta=" + tagMeta +
                ", threadSerialNumber=" + threadSerialNumber +
                ", threadObjectId=" + threadObjectId +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", threadNameStringId=" + threadNameStringId +
                ", threadGroupNameId=" + threadGroupNameId +
                ", threadParentGroupNameId=" + threadParentGroupNameId +
                '}';
    }
}
