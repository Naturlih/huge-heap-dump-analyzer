package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface Tag {
    TagMeta getTagMeta();
}
