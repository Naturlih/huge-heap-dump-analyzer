package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class AllocSite {
    private final BasicType basicType;
    private final long classSerialNumber;
    private final long stackTraceSerialNumber;
    private final long liveBytes;
    private final long liveInstances;
    private final long bytesAllocated;
    private final long instancesAllocated;

    public AllocSite(BasicType basicType, long classSerialNumber, long stackTraceSerialNumber, long liveBytes, long liveInstances, long bytesAllocated, long instancesAllocated) {
        this.basicType = basicType;
        this.classSerialNumber = classSerialNumber;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.liveBytes = liveBytes;
        this.liveInstances = liveInstances;
        this.bytesAllocated = bytesAllocated;
        this.instancesAllocated = instancesAllocated;
    }

    public BasicType getBasicType() {
        return basicType;
    }

    public long getClassSerialNumber() {
        return classSerialNumber;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public long getLiveBytes() {
        return liveBytes;
    }

    public long getLiveInstances() {
        return liveInstances;
    }

    public long getBytesAllocated() {
        return bytesAllocated;
    }

    public long getInstancesAllocated() {
        return instancesAllocated;
    }

    @Override
    public String toString() {
        return "AllocSite{" +
                "basicType=" + basicType +
                ", classSerialNumber=" + classSerialNumber +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", liveBytes=" + liveBytes +
                ", liveInstances=" + liveInstances +
                ", bytesAllocated=" + bytesAllocated +
                ", instancesAllocated=" + instancesAllocated +
                '}';
    }
}
