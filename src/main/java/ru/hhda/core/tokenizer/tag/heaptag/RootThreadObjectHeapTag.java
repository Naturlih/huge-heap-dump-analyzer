package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootThreadObjectHeapTag implements HeapTag {
    private final TagId id;
    private final long threadSerialNumber;
    private final long stackTraceSerialNumber;
    private final long offset;

    public RootThreadObjectHeapTag(TagId id, long threadSerialNumber, long stackTraceSerialNumber, long offset) {
        this.id = id;
        this.threadSerialNumber = threadSerialNumber;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_THREAD_OBJECT;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootThreadObjectHeapTag{" +
                "id=" + id +
                ", threadSerialNumber=" + threadSerialNumber +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                '}';
    }
}
