package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class HeapSummaryTag implements Tag {
    private final TagMeta tagMeta;
    private final long totalLiveBytes;
    private final long totalLiveInstances;
    private final long totalBytesAllocated;
    private final long totalInstancesAllocated;

    public HeapSummaryTag(TagMeta tagMeta, long totalLiveBytes, long totalLiveInstances, long totalBytesAllocated, long totalInstancesAllocated) {
        this.tagMeta = tagMeta;
        this.totalLiveBytes = totalLiveBytes;
        this.totalLiveInstances = totalLiveInstances;
        this.totalBytesAllocated = totalBytesAllocated;
        this.totalInstancesAllocated = totalInstancesAllocated;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getTotalLiveBytes() {
        return totalLiveBytes;
    }

    public long getTotalLiveInstances() {
        return totalLiveInstances;
    }

    public long getTotalBytesAllocated() {
        return totalBytesAllocated;
    }

    public long getTotalInstancesAllocated() {
        return totalInstancesAllocated;
    }

    @Override
    public String toString() {
        return "HeapSummaryTag{" +
                "tagMeta=" + tagMeta +
                ", totalLiveBytes=" + totalLiveBytes +
                ", totalLiveInstances=" + totalLiveInstances +
                ", totalBytesAllocated=" + totalBytesAllocated +
                ", totalInstancesAllocated=" + totalInstancesAllocated +
                '}';
    }
}
