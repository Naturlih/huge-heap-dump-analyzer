package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@HeapRoot
@LookupInHeapFile
public class RootThreadBlockHeapTag implements HeapTag {
    private final TagId id;
    private final long threadSerialNumber;
    private final long offset;

    public RootThreadBlockHeapTag(TagId id, long threadSerialNumber, long offset) {
        this.id = id;
        this.threadSerialNumber = threadSerialNumber;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.ROOT_THREAD_BLOCK;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "RootThreadBlockHeapTag{" +
                "id=" + id +
                ", threadSerialNumber=" + threadSerialNumber +
                '}';
    }
}
