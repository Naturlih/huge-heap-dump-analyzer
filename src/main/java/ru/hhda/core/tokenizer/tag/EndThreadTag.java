package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class EndThreadTag implements Tag {
    private final TagMeta tagMeta;
    private final long threadSerialNumber;

    public EndThreadTag(TagMeta tagMeta, long threadSerialNumber) {
        this.tagMeta = tagMeta;
        this.threadSerialNumber = threadSerialNumber;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    @Override
    public String toString() {
        return "EndThreadTag{" +
                "tagMeta=" + tagMeta +
                ", threadSerialNumber=" + threadSerialNumber +
                '}';
    }
}
