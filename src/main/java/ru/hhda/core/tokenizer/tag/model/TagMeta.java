package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class TagMeta {
    /**
     * LENGTH: number of bytes that follow this u4 field and belong to this record
     */
    private final TagType tagType;
    /**
     * TIME: number of microseconds since the time stamp in the header
     */
    private final long timeSinceStart;
    /**
     * LENGTH: number of bytes that follow this u4 field and belong to this record
     */
    private final long length;

    public TagMeta(TagType tagType, long timeSinceStart, long length) {
        this.tagType = tagType;
        this.timeSinceStart = timeSinceStart;
        this.length = length;
    }

    public TagType getTagType() {
        return tagType;
    }

    public long getTimeSinceStart() {
        return timeSinceStart;
    }

    public long getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "TagMeta{" +
                "tagType=" + tagType +
                ", timeSinceStart=" + timeSinceStart +
                ", length=" + length +
                '}';
    }
}
