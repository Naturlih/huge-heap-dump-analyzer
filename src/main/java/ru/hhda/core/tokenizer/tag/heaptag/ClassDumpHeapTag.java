package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.model.*;

import java.util.Arrays;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ClassDumpHeapTag implements HeapTag {
    private final TagId id;
    private final long stackTraceSerialNumber;
    private final TagId superClassId;
    private final TagId classLoaderId;
    private final TagId signersId;
    private final TagId protectionDomainId;
    private final TagId reserved1;
    private final TagId reserved2;
    private final long instanceSize;
    private final int constantPoolSize;
    private final ConstantPoolEntry[] constantPoolEntries;
    private final int staticFieldsSize;
    private final StaticFieldEntry[] staticFieldEntries;
    private final int instanceFieldsSize;
    private final InstanceFieldEntry[] instanceFieldEntries;
    private final long offset;

    public ClassDumpHeapTag(TagId id, long stackTraceSerialNumber, TagId superClassId, TagId classLoaderId,
                            TagId signersId, TagId protectionDomainId, TagId reserved1, TagId reserved2,
                            long instanceSize, int constantPoolSize, ConstantPoolEntry[] constantPoolEntries,
                            int staticFieldsSize, StaticFieldEntry[] staticFieldEntries, int instanceFieldsSize,
                            InstanceFieldEntry[] instanceFieldEntries, long offset) {
        this.id = id;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.superClassId = superClassId;
        this.classLoaderId = classLoaderId;
        this.signersId = signersId;
        this.protectionDomainId = protectionDomainId;
        this.reserved1 = reserved1;
        this.reserved2 = reserved2;
        this.instanceSize = instanceSize;
        this.constantPoolSize = constantPoolSize;
        this.constantPoolEntries = constantPoolEntries;
        this.staticFieldsSize = staticFieldsSize;
        this.staticFieldEntries = staticFieldEntries;
        this.instanceFieldsSize = instanceFieldsSize;
        this.instanceFieldEntries = instanceFieldEntries;
        this.offset = offset;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public TagId getSuperClassId() {
        return superClassId;
    }

    public TagId getClassLoaderId() {
        return classLoaderId;
    }

    public TagId getSignersId() {
        return signersId;
    }

    public TagId getProtectionDomainId() {
        return protectionDomainId;
    }

    public TagId getReserved1() {
        return reserved1;
    }

    public TagId getReserved2() {
        return reserved2;
    }

    public long getInstanceSize() {
        return instanceSize;
    }

    public int getConstantPoolSize() {
        return constantPoolSize;
    }

    public ConstantPoolEntry[] getConstantPoolEntries() {
        return constantPoolEntries;
    }

    public int getStaticFieldsSize() {
        return staticFieldsSize;
    }

    public StaticFieldEntry[] getStaticFieldEntries() {
        return staticFieldEntries;
    }

    public int getInstanceFieldsSize() {
        return instanceFieldsSize;
    }

    public InstanceFieldEntry[] getInstanceFieldEntries() {
        return instanceFieldEntries;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.CLASS_DUMP;
    }

    @Override
    public String toString() {
        return "ClassDumpHeapTag{" +
                "id=" + id +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", superClassId=" + superClassId +
                ", classLoaderId=" + classLoaderId +
                ", signersId=" + signersId +
                ", protectionDomainId=" + protectionDomainId +
                ", reserved1=" + reserved1 +
                ", reserved2=" + reserved2 +
                ", instanceSize=" + instanceSize +
                ", constantPoolSize=" + constantPoolSize +
                ", constantPoolEntries=" + Arrays.toString(constantPoolEntries) +
                ", staticFieldsSize=" + staticFieldsSize +
                ", staticFieldEntries=" + Arrays.toString(staticFieldEntries) +
                ", instanceFieldsSize=" + instanceFieldsSize +
                ", instanceFieldEntries=" + Arrays.toString(instanceFieldEntries) +
                '}';
    }
}
