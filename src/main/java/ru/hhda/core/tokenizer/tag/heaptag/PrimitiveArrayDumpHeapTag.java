package ru.hhda.core.tokenizer.tag.heaptag;

import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;
import ru.hhda.core.tokenizer.tag.model.BasicType;
import ru.hhda.core.tokenizer.tag.model.HeapTagType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@LookupInHeapFile
public class PrimitiveArrayDumpHeapTag implements HeapTag {
    private final TagId id;
    private final long stackTraceSerialNumber;
    private final long numberOfElements;
    private final BasicType basicType;
    private final long offset;
    private final byte[] value;

    public PrimitiveArrayDumpHeapTag(TagId id, long stackTraceSerialNumber, long numberOfElements, BasicType basicType, long offset, byte[] value) {
        this.id = id;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.numberOfElements = numberOfElements;
        this.basicType = basicType;
        this.offset = offset;
        this.value = value;
    }

    @Override
    public TagId getId() {
        return id;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public long getNumberOfElements() {
        return numberOfElements;
    }

    public BasicType getBasicType() {
        return basicType;
    }

    public byte[] getValue() {
        return value;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public HeapTagType getHeapTagType() {
        return HeapTagType.PRIMITIVE_ARRAY_DUMP;
    }

    @Override
    public String toString() {
        return "PrimitiveArrayDumpHeapTag{" +
                "id=" + id +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", numberOfElements=" + numberOfElements +
                ", basicType=" + basicType +
                '}';
    }
}
