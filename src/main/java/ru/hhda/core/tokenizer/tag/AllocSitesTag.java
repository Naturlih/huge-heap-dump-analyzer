package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.AllocSite;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

import java.util.Arrays;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class AllocSitesTag implements Tag {
    private final TagMeta tagMeta;
    private final int flags;
    private final long cutoffRatio;
    private final long totalLiveBytes;
    private final long totalLiveInstances;
    private final long totalBytesAllocated;
    private final long totalInstancesAllocated;
    private final long sitesCount;
    private final AllocSite[] allocSites;

    public AllocSitesTag(TagMeta tagMeta, int flags, long cutoffRatio, long totalLiveBytes, long totalLiveInstances,
                         long totalBytesAllocated, long totalInstancesAllocated, long sitesCount, AllocSite[] allocSites) {
        this.tagMeta = tagMeta;
        this.flags = flags;
        this.cutoffRatio = cutoffRatio;
        this.totalLiveBytes = totalLiveBytes;
        this.totalLiveInstances = totalLiveInstances;
        this.totalBytesAllocated = totalBytesAllocated;
        this.totalInstancesAllocated = totalInstancesAllocated;
        this.sitesCount = sitesCount;
        this.allocSites = allocSites;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public int getFlags() {
        return flags;
    }

    public long getCutoffRatio() {
        return cutoffRatio;
    }

    public long getTotalLiveBytes() {
        return totalLiveBytes;
    }

    public long getTotalLiveInstances() {
        return totalLiveInstances;
    }

    public long getTotalBytesAllocated() {
        return totalBytesAllocated;
    }

    public long getTotalInstancesAllocated() {
        return totalInstancesAllocated;
    }

    public long getSitesCount() {
        return sitesCount;
    }

    public AllocSite[] getAllocSites() {
        return allocSites;
    }

    @Override
    public String toString() {
        return "AllocSitesTag{" +
                "tagMeta=" + tagMeta +
                ", flags=" + flags +
                ", cutoffRatio=" + cutoffRatio +
                ", totalLiveBytes=" + totalLiveBytes +
                ", totalLiveInstances=" + totalLiveInstances +
                ", totalBytesAllocated=" + totalBytesAllocated +
                ", totalInstancesAllocated=" + totalInstancesAllocated +
                ", sitesCount=" + sitesCount +
                ", allocSites=" + Arrays.toString(allocSites) +
                '}';
    }
}
