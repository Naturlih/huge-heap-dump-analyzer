package ru.hhda.core.tokenizer.tag.model;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ConstantPoolEntry {
    private final int index;
    private final BasicType basicType;
    private final long value;

    public ConstantPoolEntry(int index, BasicType basicType, long value) {
        this.index = index;
        this.basicType = basicType;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public BasicType getBasicType() {
        return basicType;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ConstantPoolEntry{" +
                "index=" + index +
                ", basicType=" + basicType +
                ", value=" + value +
                '}';
    }
}
