package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagId;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

import java.util.Arrays;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StackTraceTag implements Tag {
    private final TagMeta tagMeta;
    private final long stackTraceSerialNumber;
    private final long threadSerialNumber;
    private final long numberOfFrames;
    private final TagId[] stackFrameIds;

    public StackTraceTag(TagMeta tagMeta, long stackTraceSerialNumber, long threadSerialNumber, long numberOfFrames, TagId[] stackFrameIds) {
        this.tagMeta = tagMeta;
        this.stackTraceSerialNumber = stackTraceSerialNumber;
        this.threadSerialNumber = threadSerialNumber;
        this.numberOfFrames = numberOfFrames;
        this.stackFrameIds = stackFrameIds;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getStackTraceSerialNumber() {
        return stackTraceSerialNumber;
    }

    public long getThreadSerialNumber() {
        return threadSerialNumber;
    }

    public long getNumberOfFrames() {
        return numberOfFrames;
    }

    public TagId[] getStackFrameIds() {
        return stackFrameIds;
    }

    @Override
    public String toString() {
        return "StackTraceTag{" +
                "tagMeta=" + tagMeta +
                ", stackTraceSerialNumber=" + stackTraceSerialNumber +
                ", threadSerialNumber=" + threadSerialNumber +
                ", numberOfFrames=" + numberOfFrames +
                ", stackFrameIds=" + Arrays.toString(stackFrameIds) +
                '}';
    }
}
