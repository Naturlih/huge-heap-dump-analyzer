package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.TagId;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class StringInUtf8Tag implements Tag {
    private final TagMeta tagMeta;
    private final TagId id;
    private final String string;

    public StringInUtf8Tag(TagMeta tagMeta, TagId id, String string) {
        this.tagMeta = tagMeta;
        this.id = id;
        this.string = string;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public TagId getId() {
        return id;
    }

    public String getString() {
        return string;
    }

    @Override
    public String toString() {
        return "StringInUtf8Tag{" +
                "tagMeta=" + tagMeta +
                ", id=" + id +
                ", string='" + string + '\'' +
                '}';
    }
}
