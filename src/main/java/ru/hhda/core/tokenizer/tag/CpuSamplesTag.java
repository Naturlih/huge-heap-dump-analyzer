package ru.hhda.core.tokenizer.tag;

import ru.hhda.core.tokenizer.tag.model.CpuSampleTrace;
import ru.hhda.core.tokenizer.tag.model.TagMeta;

import java.util.Arrays;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class CpuSamplesTag implements Tag {
    private final TagMeta tagMeta;
    private final long totalNumberOfSamples;
    private final CpuSampleTrace[] cpuSampleTraces;

    public CpuSamplesTag(TagMeta tagMeta, long totalNumberOfSamples, CpuSampleTrace[] cpuSampleTraces) {
        this.tagMeta = tagMeta;
        this.totalNumberOfSamples = totalNumberOfSamples;
        this.cpuSampleTraces = cpuSampleTraces;
    }

    @Override
    public TagMeta getTagMeta() {
        return tagMeta;
    }

    public long getTotalNumberOfSamples() {
        return totalNumberOfSamples;
    }

    public CpuSampleTrace[] getCpuSampleTraces() {
        return cpuSampleTraces;
    }

    @Override
    public String toString() {
        return "CpuSamplesTag{" +
                "tagMeta=" + tagMeta +
                ", totalNumberOfSamples=" + totalNumberOfSamples +
                ", cpuSampleTraces=" + Arrays.toString(cpuSampleTraces) +
                '}';
    }
}
