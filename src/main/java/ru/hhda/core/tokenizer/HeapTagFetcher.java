package ru.hhda.core.tokenizer;

import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.tokenizer.tag.heaptag.*;
import ru.hhda.core.tokenizer.tag.model.*;

import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class HeapTagFetcher {
    private final HeapReader heapReader;
    private final ReaderContext readerContext;

    public HeapTagFetcher(HeapReader heapReader, ReaderContext readerContext) {
        this.heapReader = heapReader;
        this.readerContext = readerContext;
    }

    public HeapTag getTag() throws IOException {
        long offset = heapReader.getOffset();
        HeapTagType heapTagType = getHeapTagType();
        HeapTag heapTag;
        switch (heapTagType) {
            case ROOT_UNKNOWN:
                heapTag = getRootUnknownTag(offset);
                break;
            case ROOT_JNI_GLOBAL:
                heapTag = getRootJniGlobalTag(offset);
                break;
            case ROOT_JNI_LOCAL:
                heapTag = getRootJniLocalTag(offset);
                break;
            case ROOT_JAVA_FRAME:
                heapTag = getRootJavaFrameTag(offset);
                break;
            case ROOT_NATIVE_STACK:
                heapTag = getRootNativeStackTag(offset);
                break;
            case ROOT_STICKY_CLASS:
                heapTag = getRootStickyClassTag(offset);
                break;
            case ROOT_THREAD_BLOCK:
                heapTag = getRootThreadBlockTag(offset);
                break;
            case ROOT_MONITOR_USED:
                heapTag = getRootMonitorUsedTag(offset);
                break;
            case ROOT_THREAD_OBJECT:
                heapTag = getRootThreadObjectTag(offset);
                break;
            case CLASS_DUMP:
                heapTag = getClassDumpTag(offset);
                break;
            case INSTANCE_DUMP:
                heapTag = getInstanceDumpTag(offset);
                break;
            case OBJECT_ARRAY_DUMP:
                heapTag = getObjectArrayDumpTag(offset);
                break;
            case PRIMITIVE_ARRAY_DUMP:
                heapTag = getPrimitiveArrayDumpTag(offset);
                break;
            default:
                throw new IllegalStateException("Unexpected heap tag type " + heapTagType);
        }

        return heapTag;
    }

    private HeapTag getRootUnknownTag(long offset) throws IOException {
        TagId id = heapReader.readId();

        return new RootUnknownHeapTag(id, offset);
    }

    private HeapTag getRootJniGlobalTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        TagId jniGlobalRefId = heapReader.readId();

        return new RootJniGlobalHeapTag(id, jniGlobalRefId, offset);
    }

    private HeapTag getRootJniLocalTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long threadSerialNumber = heapReader.readU4();
        long frameNumberInStackTrace = heapReader.readU4();

        return new RootJniLocalHeapTag(id, threadSerialNumber, frameNumberInStackTrace, offset);
    }

    private HeapTag getRootJavaFrameTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long threadSerialNumber = heapReader.readU4();
        long frameNumberInStackTrace = heapReader.readU4();

        return new RootJavaFrameHeapTag(id, threadSerialNumber, frameNumberInStackTrace, offset);
    }

    private HeapTag getRootNativeStackTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long threadSerialNumber = heapReader.readU4();

        return new RootNativeStackHeapTag(id, threadSerialNumber, offset);
    }

    private HeapTag getRootStickyClassTag(long offset) throws IOException {
        TagId id = heapReader.readId();

        return new RootStickyClassHeapTag(id, offset);
    }

    private HeapTag getRootThreadBlockTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long threadSerialNumber = heapReader.readU4();

        return new RootThreadBlockHeapTag(id, threadSerialNumber, offset);
    }

    private HeapTag getRootMonitorUsedTag(long offset) throws IOException {
        TagId id = heapReader.readId();

        return new RootMonitorUsedHeapTag(id, offset);
    }

    private HeapTag getRootThreadObjectTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long threadSerialNumber = heapReader.readU4();
        long stackTraceSerialNumber = heapReader.readU4();

        return new RootThreadObjectHeapTag(id, threadSerialNumber, stackTraceSerialNumber, offset);
    }

    private HeapTag getClassDumpTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        TagId superClassId = heapReader.readId();
        TagId classLoaderId = heapReader.readId();
        TagId signersId = heapReader.readId();
        TagId protectionDomainId = heapReader.readId();
        TagId reserved1 = heapReader.readId();
        TagId reserved2 = heapReader.readId();
        long instanceSize = heapReader.readU4();
        int constantPoolSize = heapReader.readU2();
        ConstantPoolEntry[] constantPoolEntries = new ConstantPoolEntry[constantPoolSize];
        for (int i = 0; i < constantPoolSize; i++) {
            int constantPoolIndex = heapReader.readU2();
            BasicType entryType = getBasicType();
            long value = getValue(entryType);

            constantPoolEntries[i] = new ConstantPoolEntry(constantPoolIndex, entryType, value);
        }
        int staticFieldSize = heapReader.readU2();
        StaticFieldEntry[] staticFieldEntries = new StaticFieldEntry[staticFieldSize];
        for (int i = 0; i < staticFieldSize; i++) {
            TagId staticFieldNameId = heapReader.readId();
            BasicType fieldType = getBasicType();
            long value = getValue(fieldType);

            staticFieldEntries[i] = new StaticFieldEntry(staticFieldNameId, fieldType, value);
        }
        int instanceFieldsSize = heapReader.readU2();
        InstanceFieldEntry[] instanceFieldEntries = new InstanceFieldEntry[instanceFieldsSize];
        for (int i = 0; i < instanceFieldsSize; i++) {
            TagId fieldNameId = heapReader.readId();
            BasicType fieldType = getBasicType();

            instanceFieldEntries[i] = new InstanceFieldEntry(fieldNameId, fieldType);
        }

        return new ClassDumpHeapTag(id, stackTraceSerialNumber, superClassId, classLoaderId, signersId,
                protectionDomainId, reserved1, reserved2, instanceSize, constantPoolSize, constantPoolEntries,
                staticFieldSize, staticFieldEntries, instanceFieldsSize, instanceFieldEntries, offset);
    }

    private HeapTag getInstanceDumpTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        TagId classObjectId = heapReader.readId();
        int length = (int) heapReader.readU4();
        byte[] value = heapReader.read(length);

        return new InstanceDumpHeapTag(id, stackTraceSerialNumber, classObjectId, length, value, offset);
    }

    private HeapTag getObjectArrayDumpTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        int length = (int) heapReader.readU4();
        TagId arrayClassId = heapReader.readId();
        TagId[] elements = new TagId[length];
        for (int i = 0; i < length; i++) {
            elements[i] = heapReader.readId();
        }

        return new ObjectArrayDumpHeapTag(id, stackTraceSerialNumber, length, arrayClassId, offset, elements);
    }

    private HeapTag getPrimitiveArrayDumpTag(long offset) throws IOException {
        TagId id = heapReader.readId();
        long stackTraceSerialNumber = heapReader.readU4();
        int length = (int) heapReader.readU4();
        BasicType elementType = getBasicType();
        byte[] value = heapReader.read(length * getTypeSize(elementType));

        return new PrimitiveArrayDumpHeapTag(id, stackTraceSerialNumber, length, elementType, offset, value);
    }

    private long getValue(BasicType basicType) throws IOException {
        switch (basicType) {
            case OBJECT:
                return heapReader.readId().getId();
            case BYTE:
            case BOOLEAN:
                return heapReader.readU1();
            case SHORT:
            case CHAR:
                return heapReader.readU2();
            case INT:
            case FLOAT:
                return heapReader.readU4();
            case LONG:
            case DOUBLE:
                return heapReader.readU8();
            case NOT_AN_ARRAY:
                throw new IllegalStateException("Tried to get value, but type is " + basicType);
            default:
                throw new IllegalStateException("Unexpected type " + basicType);
        }
    }

    private int getTypeSize(BasicType basicType) throws IOException {
        switch (basicType) {
            case OBJECT:
                return readerContext.getIdentifierSize();
            case BYTE:
            case BOOLEAN:
                return 1;
            case SHORT:
            case CHAR:
                return 2;
            case INT:
            case FLOAT:
                return 4;
            case LONG:
            case DOUBLE:
                return 8;
            case NOT_AN_ARRAY:
                throw new IllegalStateException("Tried to get value, but type is " + basicType);
            default:
                throw new IllegalStateException("Unexpected type " + basicType);
        }
    }

    private BasicType getBasicType() throws IOException {
        byte elementTypeId = (byte) heapReader.readU1();
        BasicType elementType = BasicType.fromId(elementTypeId);
        if (elementType == null) {
            throw new IllegalStateException("Cannot find element type by id " + elementTypeId);
        }

        return elementType;
    }

    private HeapTagType getHeapTagType() throws IOException {
        byte id = (byte) heapReader.readU1();
        HeapTagType heapTagType = HeapTagType.fromId(id);
        if (heapTagType == null) {
            throw new IllegalStateException("Cannot find heap tag type by id " + id);
        }

        return heapTagType;
    }
}
