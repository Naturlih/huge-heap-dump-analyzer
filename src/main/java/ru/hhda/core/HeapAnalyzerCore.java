package ru.hhda.core;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.hhda.core.config.DatabaseSetupBeanPostProcessor;
import ru.hhda.core.config.HeapAnalyzerConfiguration;
import ru.hhda.core.config.ReaderContextSetupBeanPostProcessor;
import ru.hhda.core.processing.HeapProcessor;

import java.io.File;
import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class HeapAnalyzerCore {
    private final AnnotationConfigApplicationContext ctx;
    private final ProcessingService processingService;

    public static HeapAnalyzerCore init(File heapDumpFile, File databaseFile) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.getBeanFactory().addBeanPostProcessor(new ReaderContextSetupBeanPostProcessor(heapDumpFile));
        ctx.getBeanFactory().addBeanPostProcessor(new DatabaseSetupBeanPostProcessor(databaseFile));
        ctx.register(HeapAnalyzerConfiguration.class);
        ctx.refresh();

        return new HeapAnalyzerCore(ctx);
    }

    private HeapAnalyzerCore(AnnotationConfigApplicationContext ctx) {
        this.ctx = ctx;
        this.processingService = new ProcessingServiceImpl(ctx.getBean(HeapProcessor.class));
    }

    public ProcessingService getProcessingService() {
        return processingService;
    }

    private static class ProcessingServiceImpl implements ProcessingService {
        private final HeapProcessor heapProcessor;
        private final Object startLock = new Object();
        private boolean started = false;

        public ProcessingServiceImpl(HeapProcessor heapProcessor) {
            this.heapProcessor = heapProcessor;
        }

        @Override
        public void startProcessing() throws IOException {
            synchronized (startLock) {
                if (started) {
                    throw new IllegalStateException("Processing is started already.");
                }

                started = true;
            }

            heapProcessor.processHeapDump();
        }

        @Override
        public ProcessingState getProcessingState() {
            return heapProcessor.getProcessingState();
        }
    }
}
