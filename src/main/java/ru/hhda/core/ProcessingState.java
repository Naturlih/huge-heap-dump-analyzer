package ru.hhda.core;

import ru.hhda.core.context.ProcessingStage;

/**
 * Represents current state of processing. Note that not for all {@code ProcessingStage} it is possible to calculate
 * completion. Refer to {@code ProcessingStage.canCalculateCompletion}.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ProcessingState {
    private final ProcessingStage stage;
    private final double completion;

    public ProcessingState(ProcessingStage stage, double completion) {
        this.stage = stage;
        this.completion = completion;
    }

    /**
     * Returns current stage of processing.
     * @return current stage of processing
     */
    public ProcessingStage getStage() {
        return stage;
    }

    /**
     * Returns completion of stage, if calculable, {@code Double.NaN} if not. Refer to
     * {@code ProcessingStage.canCalculateCompletion}.
     * @return completion of stage
     * @see ProcessingStage
     */
    public double getCompletion() {
        if (stage.canCalculateCompletion()) {
            return completion;
        } else {
            return Double.NaN;
        }
    }

    @Override
    public String toString() {
        return "ProcessingState{" +
                "stage=" + stage +
                ", completion=" + completion +
                '}';
    }
}
