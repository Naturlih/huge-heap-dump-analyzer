package ru.hhda.core.processing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.ProcessingState;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.processing.instance.ClassInstanceParser;
import ru.hhda.core.processing.tagprocessor.*;
import ru.hhda.core.tokenizer.HeapReader;
import ru.hhda.core.tokenizer.Tokenizer;
import ru.hhda.core.tokenizer.tag.LoadClassTag;
import ru.hhda.core.tokenizer.tag.StringInUtf8Tag;
import ru.hhda.core.tokenizer.tag.Tag;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;

import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class HeapProcessor {
    @Autowired
    private final Tokenizer tokenizer;
    @Autowired
    private final HeapReader heapReader;
    @Autowired
    private final ReaderContext readerContext;
    @Autowired
    private final ClassInstanceParser classInstanceParser;
    @Autowired
    private final LoadClassTagProcessor loadClassTagProcessor;
    @Autowired
    private final StringTagProcessor stringTagProcessor;
    @Autowired
    private final ClassHeapTagProcessor classHeapTagProcessor;
    @Autowired
    private final SaveInstancesHeapTagProcessor saveInstancesHeapTagProcessor;
    @Autowired
    private final SaveRefsHeapTagProcessor saveRefsHeapTagProcessor;
    @Autowired
    private final RootHeapTagProcessor rootHeapTagProcessor;

    private ProcessingStage processingStage;
    private double processingStageCompletion;

    public HeapProcessor(Tokenizer tokenizer, HeapReader heapReader, ReaderContext readerContext,
                         ClassInstanceParser classInstanceParser, LoadClassTagProcessor loadClassTagProcessor,
                         StringTagProcessor stringTagProcessor, ClassHeapTagProcessor classHeapTagProcessor,
                         SaveInstancesHeapTagProcessor saveInstancesHeapTagProcessor,
                         SaveRefsHeapTagProcessor saveRefsHeapTagProcessor, RootHeapTagProcessor rootHeapTagProcessor) {
        this.tokenizer = tokenizer;
        this.heapReader = heapReader;
        this.readerContext = readerContext;
        this.classInstanceParser = classInstanceParser;
        this.loadClassTagProcessor = loadClassTagProcessor;
        this.stringTagProcessor = stringTagProcessor;
        this.classHeapTagProcessor = classHeapTagProcessor;
        this.saveInstancesHeapTagProcessor = saveInstancesHeapTagProcessor;
        this.saveRefsHeapTagProcessor = saveRefsHeapTagProcessor;
        this.rootHeapTagProcessor = rootHeapTagProcessor;
        this.processingStage = ProcessingStage.NOT_STARTED;
    }

    public void processHeapDump() throws IOException {
        try {
            processMeta();
            resetReader();
            waitFinishOfMetaProcessing();
            buildClassDescriptors();
            processInstances();
            waitForFinishOfInstancesProcessing();
            changeStage(ProcessingStage.COMPLETED);
        } catch (IOException e) {
            heapReader.close();

            throw e;
        }
    }

    private void changeStage(ProcessingStage stage) {
        processingStageCompletion = 0;
        processingStage = stage;
    }

    public ProcessingState getProcessingState() {
        return new ProcessingState(processingStage, processingStageCompletion);
    }

    private void resetReader() throws IOException {
        heapReader.reset();
    }

    private void waitFinishOfMetaProcessing() {
        changeStage(ProcessingStage.FINALIZING_META);
        stringTagProcessor.waitForInsertFinish();
    }

    private void processMeta() throws IOException {
        changeStage(ProcessingStage.PROCESSING_META);
        while (heapReader.hasMore()) {
            Tag tag = tokenizer.readTag();

            if (tag instanceof StringInUtf8Tag) {
                stringTagProcessor.parseTag(((StringInUtf8Tag) tag));
            }
            if (tag instanceof LoadClassTag) {
                loadClassTagProcessor.parseTag(((LoadClassTag) tag));
            }

            updateCompletion();

            while (tokenizer.isParsingHeapDump()) {
                HeapTag heapTag = tokenizer.readHeapTag();
                if (heapTag != null) {//TODO
                    classHeapTagProcessor.processHeapTag(heapTag);
                    rootHeapTagProcessor.processHeapTag(heapTag);
                }
                updateCompletion();
            }
        }
    }

    private void buildClassDescriptors() {
        changeStage(ProcessingStage.BUILDING_CLASS_DESCRIPTORS);
        classInstanceParser.init(classHeapTagProcessor, loadClassTagProcessor,
                (completionRate) -> HeapProcessor.this.processingStageCompletion = completionRate);
    }

    private void processInstances() throws IOException {
        changeStage(ProcessingStage.PROCESSING_HEAP);
        while (heapReader.hasMore()) {
            tokenizer.readTag();
            updateCompletion();
            while (tokenizer.isParsingHeapDump()) {
                HeapTag heapTag = tokenizer.readHeapTag();
                if (heapTag != null) {
                    saveRefsHeapTagProcessor.processHeapTag(heapTag);
                    saveInstancesHeapTagProcessor.processHeapTag(heapTag);
                }
                updateCompletion();
            }
        }
    }

    private void waitForFinishOfInstancesProcessing() {
        changeStage(ProcessingStage.FINALIZING_HEAP);
        saveRefsHeapTagProcessor.waitForInsertFinish();
        saveInstancesHeapTagProcessor.waitForInsertFinish();
    }

    private void updateCompletion() {
        processingStageCompletion = (double) heapReader.getOffset() / readerContext.getHeapDumpSize();
    }
}
