package ru.hhda.core.processing.tagprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.InstanceDatabaseService;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class SaveInstancesHeapTagProcessor implements HeapTagProcessor {
    @Autowired
    private final InstanceDatabaseService instanceDatabaseService;

    public SaveInstancesHeapTagProcessor(InstanceDatabaseService instanceDatabaseService) {
        this.instanceDatabaseService = instanceDatabaseService;
    }

    @Override
    public void processHeapTag(HeapTag tag) {
        if (tag.getClass().isAnnotationPresent(LookupInHeapFile.class)) {
            instanceDatabaseService.addInstance(tag);
        }
    }

    public void waitForInsertFinish() {
        instanceDatabaseService.finishStage(ProcessingStage.FINALIZING_HEAP);
    }
}
