package ru.hhda.core.processing.tagprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.RefDatabaseService;
import ru.hhda.core.processing.instance.ClassInstanceParser;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.heaptag.InstanceDumpHeapTag;
import ru.hhda.core.tokenizer.tag.heaptag.ObjectArrayDumpHeapTag;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.util.List;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class SaveRefsHeapTagProcessor implements HeapTagProcessor {
    @Autowired
    private final ClassInstanceParser classInstanceParser;
    @Autowired
    private final RefDatabaseService refDatabaseService;

    public SaveRefsHeapTagProcessor(ClassInstanceParser classInstanceParser, RefDatabaseService refDatabaseService) {
        this.classInstanceParser = classInstanceParser;
        this.refDatabaseService = refDatabaseService;
    }

    @Override
    public void processHeapTag(HeapTag tag) {
        if (tag instanceof InstanceDumpHeapTag) {
            List<TagId> refsOut = classInstanceParser.getRefsOut(((InstanceDumpHeapTag) tag));
            refsOut.forEach(id -> refDatabaseService.addRef(tag.getId(), id));
        } else if (tag instanceof ObjectArrayDumpHeapTag) {
            for (TagId elementId : ((ObjectArrayDumpHeapTag) tag).getElements()) {
                refDatabaseService.addRef(tag.getId(), elementId);
            }
        }
    }

    public void waitForInsertFinish() {
        refDatabaseService.finishStage(ProcessingStage.FINALIZING_HEAP);
    }
}
