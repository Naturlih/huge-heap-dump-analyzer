package ru.hhda.core.processing.tagprocessor;

import ru.hhda.core.tokenizer.tag.Tag;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface TagProcessor<T extends Tag> {
    void parseTag(T tag);
}
