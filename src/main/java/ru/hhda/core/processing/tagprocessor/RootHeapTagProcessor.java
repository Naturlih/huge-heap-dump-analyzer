package ru.hhda.core.processing.tagprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.database.RootDatabaseService;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class RootHeapTagProcessor implements HeapTagProcessor {
    @Autowired
    private final RootDatabaseService rootDatabaseService;

    public RootHeapTagProcessor(RootDatabaseService rootDatabaseService) {
        this.rootDatabaseService = rootDatabaseService;
    }

    @Override
    public void processHeapTag(HeapTag tag) {
        if (tag.getClass().isAnnotationPresent(HeapRoot.class)) {
            rootDatabaseService.addInstance(tag);
        }
    }
}
