package ru.hhda.core.processing.tagprocessor;

import org.springframework.stereotype.Component;
import ru.hhda.core.tokenizer.tag.LoadClassTag;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class LoadClassTagProcessor implements TagProcessor<LoadClassTag> {
    private final Map<TagId, TagId> classToClassNameMap;

    public LoadClassTagProcessor() {
        this.classToClassNameMap = new HashMap<>();
    }

    public Map<TagId, TagId> getClassToClassNameMap() {
        return classToClassNameMap;
    }

    @Override
    public void parseTag(LoadClassTag tag) {
        classToClassNameMap.putIfAbsent(tag.getClassObjectId(), tag.getClassNameStringId());
    }
}
