package ru.hhda.core.processing.tagprocessor;

import org.springframework.stereotype.Component;
import ru.hhda.core.tokenizer.tag.heaptag.ClassDumpHeapTag;
import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class ClassHeapTagProcessor implements HeapTagProcessor {
    private final Map<TagId, ClassDumpHeapTag> idToTagMap;

    public ClassHeapTagProcessor() {
        this.idToTagMap = new HashMap<>();
    }

    @Override
    public void processHeapTag(HeapTag tag) {
        if (tag instanceof ClassDumpHeapTag) {
            idToTagMap.put(tag.getId(), ((ClassDumpHeapTag) tag));
        }
    }

    public Map<TagId, ClassDumpHeapTag> getIdToTagMap() {
        return idToTagMap;
    }
}
