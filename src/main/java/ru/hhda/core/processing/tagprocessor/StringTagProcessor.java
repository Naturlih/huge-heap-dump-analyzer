package ru.hhda.core.processing.tagprocessor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ProcessingStage;
import ru.hhda.core.database.StringDatabaseService;
import ru.hhda.core.tokenizer.tag.StringInUtf8Tag;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class StringTagProcessor implements TagProcessor<StringInUtf8Tag> {
    @Autowired
    private final StringDatabaseService stringDatabaseService;

    public StringTagProcessor(StringDatabaseService stringDatabaseService) {
        this.stringDatabaseService = stringDatabaseService;
    }

    @Override
    public void parseTag(StringInUtf8Tag tag) {
        stringDatabaseService.addString(tag.getId(), tag.getString());
    }

    public void waitForInsertFinish() {
        stringDatabaseService.finishStage(ProcessingStage.FINALIZING_META);
    }
}
