package ru.hhda.core.processing.tagprocessor;

import ru.hhda.core.tokenizer.tag.heaptag.HeapTag;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface HeapTagProcessor {
    void processHeapTag(HeapTag tag);
}
