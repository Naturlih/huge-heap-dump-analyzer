package ru.hhda.core.processing.instance.field;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class FieldParserContext {
    private final byte[] value;
    private int offset;

    public FieldParserContext(byte[] value) {
        this.value = value;
        this.offset = 0;
    }

    long read(int count) {
        if (count > 8) {
            throw new IllegalArgumentException("Count cannot be more than 8 (" + count + ")");
        } else if (count < 0) {
            throw new IllegalArgumentException("Count is less than zero (" + count + ")");
        }

        long result = 0;
        for (int i = 0; i < count; i++) {
            result = (result << 8) ^ Byte.toUnsignedInt(value[offset]);
            offset++;
        }

        return result;
    }
}
