package ru.hhda.core.processing.instance.field;

import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.tokenizer.tag.model.BasicType;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class FieldParser {
    private final String fieldName;
    private final BasicType basicType;
    private final int fieldSize;

    public static FieldParser getForBasicType(BasicType basicType, String name, ReaderContext readerContext) {
        return new FieldParser(name, basicType, readerContext.getTypeSize(basicType));
    }

    private FieldParser(String fieldName, BasicType basicType, int fieldSize) {
        this.fieldName = fieldName;
        this.basicType = basicType;
        this.fieldSize = fieldSize;
    }

    public long getValue(FieldParserContext fieldParserContext) {
        return fieldParserContext.read(fieldSize);
    }

    public String getFieldName() {
        return fieldName;
    }

    public BasicType getBasicType() {
        return basicType;
    }
}
