package ru.hhda.core.processing.instance.field;

import ru.hhda.core.tokenizer.tag.model.TagId;

import java.util.List;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ClassInstance {
    private final String className;
    private final TagId instanceId;
    private final List<Field> fields;

    public ClassInstance(String className, TagId instanceId, List<Field> fields) {
        this.className = className;
        this.instanceId = instanceId;
        this.fields = fields;
    }

    public String getClassName() {
        return className;
    }

    public TagId getInstanceId() {
        return instanceId;
    }

    public List<Field> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "ClassInstance{" +
                "className='" + className + '\'' +
                ", fields=" + fields +
                '}';
    }
}
