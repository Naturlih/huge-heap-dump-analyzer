package ru.hhda.core.processing.instance.field;

import ru.hhda.core.tokenizer.tag.model.BasicType;
import ru.hhda.core.tokenizer.tag.model.TagId;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class Field {
    private final String fieldName;
    private final long value;
    private final BasicType basicType;

    public Field(String fieldName, long value, BasicType basicType) {
        this.fieldName = fieldName;
        this.value = value;
        this.basicType = basicType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getVisibleValue() {
        return String.valueOf(value);
    }

    public BasicType getBasicType() {
        return basicType;
    }

    public TagId getAsId() {
        if (basicType != BasicType.OBJECT) {
            throw new IllegalStateException("Value is not id, but " + basicType);
        }

        return new TagId(value);
    }

    @Override
    public String toString() {
        return "Field{" +
                "fieldName='" + fieldName + '\'' +
                ", value=" + value +
                '}';
    }
}
