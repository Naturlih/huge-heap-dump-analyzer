package ru.hhda.core.processing.instance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.hhda.core.context.ReaderContext;
import ru.hhda.core.database.StringDatabaseService;
import ru.hhda.core.database.model.InstanceDump;
import ru.hhda.core.processing.instance.field.ClassInstance;
import ru.hhda.core.processing.instance.field.Field;
import ru.hhda.core.processing.instance.field.FieldParser;
import ru.hhda.core.processing.instance.field.FieldParserContext;
import ru.hhda.core.processing.tagprocessor.ClassHeapTagProcessor;
import ru.hhda.core.processing.tagprocessor.LoadClassTagProcessor;
import ru.hhda.core.tokenizer.HeapReader;
import ru.hhda.core.tokenizer.HeapTagFetcher;
import ru.hhda.core.tokenizer.tag.heaptag.*;
import ru.hhda.core.tokenizer.tag.model.BasicType;
import ru.hhda.core.tokenizer.tag.model.InstanceFieldEntry;
import ru.hhda.core.tokenizer.tag.model.TagId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class ClassInstanceParser {
    @Autowired
    private final ReaderContext readerContext;
    @Autowired
    private final StringDatabaseService stringDatabaseService;
    private final Map<TagId, ClassDefinition> classFieldsParserMap;
    private final Map<TagId, TagId> classToClassNameMap;

    public ClassInstanceParser(ReaderContext readerContext, StringDatabaseService stringDatabaseService) {
        this.readerContext = readerContext;
        this.stringDatabaseService = stringDatabaseService;
        this.classFieldsParserMap = new HashMap<>();
        this.classToClassNameMap = new HashMap<>();
    }

    public void init(ClassHeapTagProcessor classHeapTagProcessor, LoadClassTagProcessor loadClassTagProcessor,
                     Consumer<Double> completionRateConsumer) {
        this.classToClassNameMap.putAll(loadClassTagProcessor.getClassToClassNameMap());
        Map<TagId, ClassDumpHeapTag> idToTagMap = classHeapTagProcessor.getIdToTagMap();
        long mapSize = idToTagMap.size();
        long proceedCount = 0L;
        for (Map.Entry<TagId, ClassDumpHeapTag> entry : idToTagMap.entrySet()) {
            TagId classObjectId = entry.getKey();
            addFields(classObjectId, entry.getValue().getSuperClassId(), entry.getValue().getInstanceFieldEntries());
            completionRateConsumer.accept(((double) mapSize) / ((double) proceedCount));
        }
    }

    public void addFields(TagId classTagId, TagId superclassTagId, InstanceFieldEntry[] fields) {
        List<FieldParser> parsers = new ArrayList<>();

        for (InstanceFieldEntry fieldEntry : fields) {
            String fieldName = stringDatabaseService.getString(fieldEntry.getFieldNameId());
            parsers.add(FieldParser.getForBasicType(fieldEntry.getBasicType(), fieldName, readerContext));
        }
        if (classFieldsParserMap.containsKey(classTagId)) {
            throw new IllegalArgumentException("Already contains definition for " + classTagId);
        }

        classFieldsParserMap.put(classTagId, new ClassDefinition(parsers, superclassTagId));
    }

    public ClassInstance parseInstance(InstanceDump instanceDump) throws IOException {
        HeapTag tag = readTag(instanceDump);
        if (tag instanceof InstanceDumpHeapTag) {
            return parseInstanceDumpHeapTag((InstanceDumpHeapTag) tag);
        } else if (tag instanceof ObjectArrayDumpHeapTag) {
            return parseObjectArrayDumpHeapTag(((ObjectArrayDumpHeapTag) tag));
        } else if (tag instanceof PrimitiveArrayDumpHeapTag) {
            return parsePrimitiveArrayDumpHeapTag(((PrimitiveArrayDumpHeapTag) tag));
        } else {
            throw new IllegalArgumentException("Is not instance dump heap tag " + instanceDump);
        }
    }

    private ClassInstance parseInstanceDumpHeapTag(InstanceDumpHeapTag instanceDumpHeapTag) {
        List<Field> fields = new ArrayList<>();
        FieldParserContext fieldParserContext = new FieldParserContext(instanceDumpHeapTag.getValue());
        ClassDefinition classDefinition = classFieldsParserMap.get(instanceDumpHeapTag.getClassObjectId());
        while (classDefinition != null) {
            for (FieldParser fieldParser : classDefinition.fieldParserList) {
                fields.add(new Field(fieldParser.getFieldName(), fieldParser.getValue(fieldParserContext), fieldParser.getBasicType())); //TODO
            }

            classDefinition = classFieldsParserMap.get(classDefinition.superclassTagId);
        }

        TagId classNameStringId = classToClassNameMap.get(instanceDumpHeapTag.getClassObjectId());

        return new ClassInstance(stringDatabaseService.getString(classNameStringId), instanceDumpHeapTag.getId(), fields);
    }

    private ClassInstance parsePrimitiveArrayDumpHeapTag(PrimitiveArrayDumpHeapTag tag) {
        FieldParserContext fieldParserContext = new FieldParserContext(tag.getValue());
        String arrayClassName = tag.getBasicType().name() + "[]";
        FieldParser fieldParser = FieldParser.getForBasicType(tag.getBasicType(), tag.getBasicType().name(), readerContext);
        List<Field> fields = new ArrayList<>();
        for (int i = 0; i < tag.getNumberOfElements(); i++) {
            fields.add(new Field(fieldParser.getFieldName(), fieldParser.getValue(fieldParserContext), fieldParser.getBasicType()));
        }

        return new ClassInstance(arrayClassName, tag.getId(), fields);
    }

    private ClassInstance parseObjectArrayDumpHeapTag(ObjectArrayDumpHeapTag tag) {
        TagId[] elementIds = tag.getElements();
        List<Field> fields = new ArrayList<>(elementIds.length);
        TagId classNameStringId = classToClassNameMap.get(tag.getArrayClassObjectId());
        String arrayClassName = stringDatabaseService.getString(classNameStringId);
        String elementClassName = arrayClassName.substring(2, arrayClassName.length() - 1); //to remove "[B" prefix
        for (TagId elementId : elementIds) {
            fields.add(new Field(elementClassName, elementId.getId(), BasicType.OBJECT));
        }

        return new ClassInstance(arrayClassName, tag.getId(), fields);
    }

    public List<TagId> getRefsOut(InstanceDumpHeapTag instanceDumpHeapTag) {
        List<TagId> refsOut = new ArrayList<>();
        FieldParserContext fieldParserContext = new FieldParserContext(instanceDumpHeapTag.getValue());
        ClassDefinition classDefinition = classFieldsParserMap.get(instanceDumpHeapTag.getClassObjectId());
        while (classDefinition != null) {
            for (FieldParser fieldParser : classDefinition.fieldParserList) {
                long value = fieldParser.getValue(fieldParserContext);

                if (fieldParser.getBasicType() == BasicType.OBJECT) {
                    refsOut.add(new TagId(value));
                }
            }

            classDefinition = classFieldsParserMap.get(classDefinition.superclassTagId);
        }

        return refsOut;
    }

    private HeapTag readTag(InstanceDump instanceDump) throws IOException {
        HeapReader heapReader = new HeapReader(readerContext);
        heapReader.init();
        heapReader.skip(instanceDump.getOffset());
        HeapTagFetcher heapTagFetcher = new HeapTagFetcher(heapReader, readerContext);

        return heapTagFetcher.getTag();
    }

    private static class ClassDefinition {
        List<FieldParser> fieldParserList;
        TagId superclassTagId;

        public ClassDefinition(List<FieldParser> fieldParserList, TagId superclassTagId) {
            this.fieldParserList = fieldParserList;
            this.superclassTagId = superclassTagId;
        }
    }
}
