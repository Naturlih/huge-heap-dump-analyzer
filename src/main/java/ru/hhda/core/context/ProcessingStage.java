package ru.hhda.core.context;

/**
 * Stage of heap processing.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public enum ProcessingStage {
    /**
     * Stage before invocation of {@code ProcessingService.startProcessing()}.
     * @see ru.hhda.core.ProcessingService
     * @see ru.hhda.core.HeapAnalyzerCore
     */
    NOT_STARTED(false),
    /**
     * First scan of heap dump.
     * Fetches meta strings like class/field names.
     * Fetches raw class descriptions.
     * @see ru.hhda.core.tokenizer.tag.StringInUtf8Tag
     * @see ru.hhda.core.processing.tagprocessor.StringTagProcessor
     * @see ru.hhda.core.tokenizer.tag.LoadClassTag
     * @see ru.hhda.core.processing.tagprocessor.LoadClassTagProcessor
     */
    PROCESSING_META(true),
    /**
     * Build indices of meta strings table.
     * @see ru.hhda.core.processing.tagprocessor.StringTagProcessor
     */
    FINALIZING_META(false),
    /**
     * Builds class descriptors to parse raw instances from heap dump.
     * @see ru.hhda.core.processing.instance.ClassInstanceParser
     */
    BUILDING_CLASS_DESCRIPTORS(true),
    /**
     * Second scan of heap dump.
     * Fills JVM roots table.
     * Adds instances information.
     * Adds refs between instances.
     * @see ru.hhda.core.tokenizer.tag.heaptag.annotation.HeapRoot
     * @see ru.hhda.core.processing.tagprocessor.RootHeapTagProcessor
     * @see ru.hhda.core.tokenizer.tag.heaptag.annotation.LookupInHeapFile
     * @see ru.hhda.core.processing.tagprocessor.SaveInstancesHeapTagProcessor
     * @see ru.hhda.core.processing.tagprocessor.SaveRefsHeapTagProcessor
     */
    PROCESSING_HEAP(true),
    /**
     * Builds indices on roots table.
     * Builds indices on instances table.
     * Builds indices on refs table.
     * @see ru.hhda.core.processing.tagprocessor.RootHeapTagProcessor
     * @see ru.hhda.core.processing.tagprocessor.SaveInstancesHeapTagProcessor
     * @see ru.hhda.core.processing.tagprocessor.SaveRefsHeapTagProcessor
     */
    FINALIZING_HEAP(false),
    /**
     * Heap file is proceed and ready to be analyzed.
     */
    COMPLETED(false);

    private final boolean canCalculateCompletion;

    ProcessingStage(boolean canCalculateCompletion) {
        this.canCalculateCompletion = canCalculateCompletion;
    }

    public boolean canCalculateCompletion() {
        return canCalculateCompletion;
    }
}
