package ru.hhda.core.context;

import org.springframework.stereotype.Component;
import ru.hhda.core.tokenizer.tag.model.BasicType;

import java.io.File;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Component
public class ReaderContext {
    /**
     * An initial NULL terminated series of bytes representing the format name and version, in this implementation
     * and historically, the string "JAVA PROFILE 1.0.1" (18 u1 bytes) followed by a NULL byte.
     * If the TAG "HEAP DUMP SEGMENT" is used this string will be "JAVA PROFILE 1.0.2".
     */
    private String header;
    /**
     * Size of identifiers. Identifiers are used to represent UTF8 strings, objects, stack traces, etc.
     * They can have the same size as host pointers or sizeof(void*), but are not required to be.
     */
    private int identifierSize;
    /**
     * Number of milliseconds since 0:00 GMT, 1/1/70
     */
    private long jvmStartTime;
    private long heapDumpSize;
    private File heapDumpFile;

    public void update(String header, int identifierSize, long jvmStartTime) {
        this.header = header;
        this.identifierSize = identifierSize;
        this.jvmStartTime = jvmStartTime;
    }

    public void setHeapDumpFile(File heapDumpFile) {
        this.heapDumpFile = heapDumpFile;
        this.heapDumpSize = heapDumpFile.length();
    }

    public String getHeader() {
        return header;
    }

    public int getIdentifierSize() {
        return identifierSize;
    }

    public long getJvmStartTime() {
        return jvmStartTime;
    }

    public long getHeapDumpSize() {
        return heapDumpSize;
    }

    public File getHeapDumpFile() {
        return heapDumpFile;
    }

    public int getTypeSize(BasicType basicType) {
        switch (basicType) {
            case OBJECT:
                return getIdentifierSize();
            case BYTE:
            case BOOLEAN:
                return 1;
            case SHORT:
            case CHAR:
                return 2;
            case INT:
            case FLOAT:
                return 4;
            case LONG:
            case DOUBLE:
                return 8;
            case NOT_AN_ARRAY:
                throw new IllegalStateException("Tried to get value, but type is " + basicType);
            default:
                throw new IllegalStateException("Unexpected type " + basicType);
        }
    }
}
