package ru.hhda.core;

import java.io.IOException;

/**
 * Service, which control execution of heap dump processing. Performs all needed work to parse dump, fill database file,
 * notify clients about state of processing.
 *
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public interface ProcessingService {
    /**
     * Starts heap processing. Blocks thread while heap is being proceed and returns when it is ready to analyze.
     *
     * @throws IllegalStateException if already started or proceed
     * @throws IOException if thrown while heap processing
     */
    void startProcessing() throws IOException;

    /**
     * Returns current state of processing. Note that not for all {@code ProcessingStage} it is possible to calculate
     * processing completion rate. Thread safe.
     *
     * @return state of processing with stage type and completion (if it is possible to obtain one)
     */
    ProcessingState getProcessingState();
}
