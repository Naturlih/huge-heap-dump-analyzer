package ru.hhda.core.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.hhda.core.processing.HeapProcessor;
import ru.hhda.core.processing.instance.ClassInstanceParser;
import ru.hhda.core.tokenizer.HeapReader;
import ru.hhda.core.tokenizer.TagFetcher;
import ru.hhda.core.tokenizer.Tokenizer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
@Configuration
@ComponentScan(basePackages = {"ru.hhda.core.database", "ru.hhda.core.context"},
        basePackageClasses = {HeapProcessor.class, HeapReader.class, TagFetcher.class, Tokenizer.class, ClassInstanceParser.class})
@EnableTransactionManagement(proxyTargetClass = true)
public class HeapAnalyzerConfiguration {
    @Bean
    public DataSource dataSource() {
        return new BasicDataSource();
    }

    @Bean
    public Connection connection() {
        try {
            return dataSource().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
