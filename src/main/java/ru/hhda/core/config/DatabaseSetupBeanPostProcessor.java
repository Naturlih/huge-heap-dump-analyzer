package ru.hhda.core.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.io.File;
import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class DatabaseSetupBeanPostProcessor implements BeanPostProcessor {
    private final File databaseFile;

    public DatabaseSetupBeanPostProcessor(File databaseFile) {
        this.databaseFile = databaseFile;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BasicDataSource) {
            ((BasicDataSource) bean).setUrl("jdbc:sqlite:" + databaseFile.getAbsolutePath());
            if (databaseFile.exists()) {
                try {
                    databaseFile.delete();
                    databaseFile.createNewFile();
                } catch (IOException e) {
                    throw new BeansException("Cannot recreate database file", e) {};
                }
            }
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
