package ru.hhda.core.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import ru.hhda.core.context.ReaderContext;

import java.io.File;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com)
 */
public class ReaderContextSetupBeanPostProcessor implements BeanPostProcessor {
    private final File heapDumpFile;

    public ReaderContextSetupBeanPostProcessor(File heapDumpFile) {
        this.heapDumpFile = heapDumpFile;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof ReaderContext) {
            ((ReaderContext) bean).setHeapDumpFile(heapDumpFile);
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }
}
