DROP TABLE IF EXISTS refs;
DROP TABLE IF EXISTS strings;
DROP TABLE IF EXISTS instanceDump;
DROP TABLE IF EXISTS heapRoot;

CREATE TABLE ref (
    refFrom BIGINT NOT NULL,
    refTo BIGINT NOT NULL
)

CREATE INDEX refs_refFrom ON refs (refFrom);
CREATE INDEX refs_refTo ON refs (refTo);

CREATE TABLE strings (
    id BIGINT NOT NULL PRIMARY KEY,
    value TEXT
)

CREATE TABLE instanceDump (
    objectId BIGINT NOT NULL PRIMARY KEY,
    tagType BYTE NOT NULL,
    offset BIGINT NOT NULL
)

CREATE TABLE heapRoot (
    objectId BIGINT NOT NULL PRIMARY KEY,
    tagType BYTE NOT NULL
)